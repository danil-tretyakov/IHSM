package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "family_members")
public class FamilyMember extends BaseEntity {

    @Column
    private String name;

    @Column
    private String surname;

    @Column
    private String patronymic;

    @Column
    private String relation;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private LocalDate birthDate;

    @ManyToOne
    private Employee employee;
}
