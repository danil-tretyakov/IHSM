package com.ihsm.esdp.news.controllers;

import com.ihsm.esdp.exceptions.ResourceNotFoundException;
import com.ihsm.esdp.news.dtos.NewsDto;
import com.ihsm.esdp.news.dtos.NewsPostDto;
import com.ihsm.esdp.news.dtos.NewsUpdateDto;
import com.ihsm.esdp.news.services.NewsService;
import com.ihsm.esdp.news.entities.News;
import com.ihsm.esdp.users.UserRole;
import com.ihsm.esdp.users.security.MySecurityUser;
import com.ihsm.esdp.utility.PaginationService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class NewsController {

    private final ModelMapper modelMapper = new ModelMapper();

    private final NewsService newsService;

    private final PaginationService paginationService;

    @GetMapping
    public String showFiveLastNews(Model model){
        model.addAttribute("all", false);
        model.addAttribute("ourLife", false);
        model.addAttribute("news", newsService.getLastFiveNews().stream()
                .map(news -> modelMapper.map(news, NewsDto.class))
                .collect(Collectors.toList()));
        return "news/news";
    }

    @GetMapping("/news/all")
    public String showAllNewsWithPageable(Model model,
                                          @PageableDefault(size = 2) Pageable pageable,
                                          @RequestParam(defaultValue = "false") boolean ourLife){
        Page<News> newsWithPages = newsService.getAllNewsByPages(pageable);
        model.addAttribute("news", newsWithPages.getContent().stream()
                .map(news -> modelMapper.map(news, NewsDto.class))
                .collect(Collectors.toList()));
        model.addAttribute("all", true);

        this.paginationService.setPaginationVariablesInModel(model, newsWithPages.getPageable(), newsWithPages.getTotalPages());
        model.addAttribute("ourLife", ourLife);
        return "news/news";
    }

    @GetMapping("/news/create")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String showPageWithNewsCreation(Model model) {
        model.addAttribute("post", new NewsPostDto());
        return "news/createpost";
    }

    @PostMapping("/news/create")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String createNewsPost(RedirectAttributes attributes,
                                 @Valid @ModelAttribute(name = "newPost") NewsPostDto newsDto,
                                 BindingResult bindingResult, Authentication auth){
        attributes.addFlashAttribute("post", newsDto);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/news/create";
        }
        try{
            MySecurityUser user = (MySecurityUser) auth.getPrincipal();
            this.newsService.createNewPost(newsDto, user);

        }catch (IOException ex){
            ex.printStackTrace();
            attributes.addFlashAttribute("post", newsDto);
            attributes.addFlashAttribute("error", "Изображение обязательно!");
            return "redirect:/news/create";
        }

        attributes.addFlashAttribute("news", newsService.getLastFiveNews().stream()
                .map(news -> modelMapper.map(news, NewsDto.class))
                .collect(Collectors.toList()));

        return "redirect:/";
    }

    @GetMapping("/news/{newsId}")
    public String getPost(Model model, @PathVariable Long newsId,
                          @PageableDefault(size = 5) Pageable pageable,
                          @RequestParam(defaultValue = "false") boolean all,
                          @RequestParam(defaultValue = "false") boolean ourLife,
                          @RequestParam(defaultValue = "false") boolean change,
                          Authentication authentication){
        if (model.containsAttribute("all") || model.containsAttribute("ourLife") || model.containsAttribute("change") || model.containsAttribute("post")){
            return "news/fullpostinfo";
        }
        News post = newsService.getPostById(newsId).orElseThrow(ResourceNotFoundException::new);
        model.addAttribute("all", all);
        model.addAttribute("pages", pageable);
        model.addAttribute("ourLife", ourLife);
        MySecurityUser user = (MySecurityUser) authentication.getPrincipal();
        if (change && user.getUser().getUserRole() != UserRole.ROLE_USER) {
            model.addAttribute("post", modelMapper.map(post, NewsUpdateDto.class));
            return "news/updateInfo";
        }
        model.addAttribute("post", modelMapper.map(post, NewsDto.class));
        return "news/fullpostinfo";
    }

    @PostMapping("/news/{newsId}/change")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String updatePost(Model model, RedirectAttributes attrs, @PathVariable Long newsId,
                             @Valid @ModelAttribute(name = "newPost") NewsUpdateDto newsDto,
                             @PageableDefault(size = 5) Pageable pageable,
                             @RequestParam(defaultValue = "false") boolean ourLife,
                             @RequestParam(defaultValue = "false") boolean all){
        try{
            this.newsService.updatePost(newsId, newsDto);
        }catch (IOException exception){
            model.addAttribute("post", newsDto);
            model.addAttribute("all", all);
            model.addAttribute("pages", pageable);
            model.addAttribute("ourLife", ourLife);
            return "news/updateInfo";
        }
        attrs.addFlashAttribute("post",
                modelMapper.map(newsService.getPostById(newsId).orElseThrow(ResourceNotFoundException::new), NewsDto.class));
        attrs.addFlashAttribute("all", all);
        attrs.addFlashAttribute("ourLife", ourLife);
        attrs.addFlashAttribute("pages", pageable);
        return "redirect:/news/" + newsId;
    }

    @GetMapping("/news/our-life")
    public String showSectionOurLife(Model model, @PageableDefault(size = 5) Pageable pageable){
        Page<News> ourLifeNews = this.newsService.getOurLifeNews(pageable);
        model.addAttribute("news",ourLifeNews.getContent().stream()
                .map(news -> modelMapper.map(news, NewsDto.class))
                .collect(Collectors.toList()));
        this.paginationService.setPaginationVariablesInModel(model, ourLifeNews.getPageable(), ourLifeNews.getTotalPages());
        model.addAttribute("ourLife", true);
        model.addAttribute("all", false);
        return "news/news";
    }

    @PostMapping("/news/delete/{postId}")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String deleteNewsPost(RedirectAttributes attrs, @PathVariable Long postId,
                                 @PageableDefault(size = 5) Pageable pageable,
                                 @RequestParam(defaultValue = "false") boolean all,
                                 @RequestParam(defaultValue = "false") boolean ourLife){
        this.newsService.deletePostById(postId);
        attrs.addFlashAttribute("pages", pageable);
        attrs.addFlashAttribute("ourLife", ourLife);
        attrs.addFlashAttribute("all", all);
        if (ourLife){
            return "redirect:/news/our-life/?size=" + pageable.getPageSize() + "&page=" + pageable.getPageNumber();
        }
        else if (all){
            return "redirect:/news/all/?size=" + pageable.getPageSize() + "&page=" + pageable.getPageNumber();
        }


        return "redirect:/";
    }
}
