package com.ihsm.esdp.users.repositories.employeeActionsRepositories;

import com.ihsm.esdp.users.information.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    List<Transaction> findAllByEmployee_Id(Long id);

    Optional<Transaction> findByDateOfTransactionAndEmployee_Id(LocalDate date, Long id);
}
