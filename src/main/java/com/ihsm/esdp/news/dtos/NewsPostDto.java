package com.ihsm.esdp.news.dtos;

import com.ihsm.esdp.news.enums.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Data
@Builder

@NoArgsConstructor
@AllArgsConstructor
public class NewsPostDto {

    @NotNull
    private String title;

    @NotNull
    private String text;

    @NotNull
    private MultipartFile image;

    @NotNull
    private Category category;
}
