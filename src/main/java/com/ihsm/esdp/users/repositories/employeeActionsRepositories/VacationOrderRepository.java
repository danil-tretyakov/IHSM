package com.ihsm.esdp.users.repositories.employeeActionsRepositories;

import com.ihsm.esdp.users.information.VacationOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VacationOrderRepository extends JpaRepository<VacationOrder, Long> {
}
