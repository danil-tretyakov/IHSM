package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "vacations")
public class Vacation extends BaseEntity {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private LocalDate startVacation;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private LocalDate endVacation;

    @ManyToOne
    private Employee employee;
}
