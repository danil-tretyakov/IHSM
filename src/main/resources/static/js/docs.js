function initButtonDocs() {
    const buttonOfDepartments = document.getElementById("button-docs");
    buttonOfDepartments.addEventListener("click", function (event) {
        event.preventDefault();
        createListOfDocs();
    });
}

function createListOfDocs() {
    if (document.getElementsByClassName("docs-link").length === 0){

        const settings ={
            mode: 'no-cors',
            method: 'GET'
        };
        fetch("http://192.168.8.51/documents/all", settings).then(function (response) {return response.text()})
            .then(function (text) {
                try{
                    const depsList = JSON.parse(text);
                    addDocs(depsList);
                }catch (e) {
                    fetch("http://192.168.8.51:8081/documents/all", settings).then(function (response) {return response.text()})
                        .then(function (text) {
                            const depsList = JSON.parse(text);
                            addDocs(depsList);
                        });
                }
            });
    }
}

function addDocs(docsList) {
    const docsLi = document.getElementById("documents");

    for (let i = 0; i < docsList.length; i++) {
        const newLink = document.createElement("div");
        newLink.innerHTML=`
        <a href="/documents/category/${docsList[i].id}" class="accordion-links text-dark">${docsList[i].categoryName}</a>
        `;
        newLink.classList.add("docs-link");
        newLink.classList.add("w-100");
        docsLi.append(newLink);
    }
}
