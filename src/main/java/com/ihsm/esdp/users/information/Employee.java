package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.users.User;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity(name = "employee")
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "employees")
public class Employee extends BaseEntity {

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    private String patronymic;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Subject subject;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Department department;

    @ManyToOne(cascade = CascadeType.MERGE)
    private EmployeePosition employeePosition;

    @Enumerated(EnumType.STRING)
    @Column
    private TypeOfEmployment typeOfEmployment;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<ScienceDegree> scienceDegree;

    @Enumerated(EnumType.STRING)
    @Column
    private ScienceTitle scienceTitle;

    @OneToOne(cascade = CascadeType.ALL)
    private PassportData passportData;

    @Column
    private String militaryRank;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<Education> educations;

    @Column
    private Integer workExperience;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(columnDefinition = "DATE")
    private LocalDate startWork;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(columnDefinition = "DATE")
    private LocalDate dismissalDate;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<Employment> employments;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<Award> awards;

    @Enumerated(EnumType.STRING)
    @Column
    private Gender gender;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(columnDefinition = "DATE")
    private LocalDate birthDate;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<FamilyMember> family;

    @Enumerated(EnumType.STRING)
    @Column
    private MaritalStatus maritalStatus;

    @Column
    private String Address;

    @Column
    private String roomNumber;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<Vacation> vacation;

    @Column
    private String phoneNumber;

    @Column
    private String imagePath;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<Transaction> transactions;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<VacationOrder> vacationOrders;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<Truancy> truancyList;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<BusinessTrip> businessTrips;
}
