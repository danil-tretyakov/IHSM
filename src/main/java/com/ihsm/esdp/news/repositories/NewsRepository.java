package com.ihsm.esdp.news.repositories;

import com.ihsm.esdp.news.enums.Category;
import com.ihsm.esdp.news.entities.News;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface NewsRepository extends JpaRepository<News, Long> {

    @Transactional
    @Modifying
    @Query("update News n set n.title = ?2, n.text=?3, n.imagePath=?4 where n.id=?1")
    void updatePostData(Long id, String title, String text, String path);

    Page<News>findByCategory(Pageable pageable, Category category);

    List<News> findAllByTitleIsContaining(String title);
}
