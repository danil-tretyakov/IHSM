window.onload = function addListenersOnTheAddFormOrInputsButtons() {
    addFeaturesWithEducationForms();
    addFeaturesWithDegreesInputs();
    addFeaturesWithFamilyForms();
    addFeaturesWithAwardForms();
    addFeaturesWithEmploymentsList();
    addFeaturesWithEmployeeActions();
}

function addFeaturesWithEducationForms() {
    const buttonAddForm = document.getElementById("create-new-edu-form");
    if (document.getElementsByClassName("education-form").length !== 0){
        const divWithExistingInfo = document.getElementsByClassName("education-form");
        for (let i = 0; i < divWithExistingInfo.length; i++) {
            divWithExistingInfo[i].lastElementChild.firstElementChild.addEventListener("click", function () {
                divWithExistingInfo[i].firstElementChild.setAttribute("value", "true");
                divWithExistingInfo[i].style.display = "none";
            });
        }
    }
    buttonAddForm.addEventListener("click", function () {
        let educationFormsSize = document.getElementsByClassName("education-form").length;
        const divWithAddButton = document.getElementById("button-add-new-form-div");
        createNewForm(divWithAddButton, educationFormsSize);
    });
}

function addFeaturesWithDegreesInputs() {
    const buttonAddScienceDegreeInput = document.getElementById("add-input-degree-button");

    const scienceDegreesInputs = document.getElementsByClassName("degree-input");
    if (scienceDegreesInputs.length !== 0){
        for (let i = 0; i < scienceDegreesInputs.length; i++) {
            scienceDegreesInputs[i].lastElementChild.addEventListener("click", function () {
                scienceDegreesInputs[i].firstElementChild.setAttribute("value", "true");
                scienceDegreesInputs[i].style.display = "none";
            });
        }
    }

    buttonAddScienceDegreeInput.addEventListener("click", function () {
        createNewDegreeInput(document.getElementsByClassName("degree-input").length);
    });
}

function addFeaturesWithFamilyForms() {
    const buttonAddMemberForm = document.getElementById("create-new-family-form");

    const familyMembersForms = document.getElementsByClassName("family-member-block");
    if (familyMembersForms.length !== 0){
        for (let i = 0; i < familyMembersForms.length; i++) {
            familyMembersForms[i].children[1].lastElementChild.firstElementChild.addEventListener("click",function () {
                familyMembersForms[i].firstElementChild.firstElementChild.setAttribute("value", "true");
                familyMembersForms[i].style.display = "none";
            });
        }
    }

    buttonAddMemberForm.addEventListener("click", function () {
        createNewFamilyForm(document.getElementsByClassName("family-member-block").length, document.getElementById("add-family-div-form"));
    });
}

function addFeaturesWithAwardForms() {
    const buttonAddNewAwardForm = document.getElementById("create-new-award-form");

    if (document.getElementsByClassName("award-form").length !== 0){
        const divWithAwardForm = document.getElementsByClassName("award-form");
        for (let i = 0; i < divWithAwardForm.length; i++) {
           divWithAwardForm[i].lastElementChild.addEventListener("click", function () {
               divWithAwardForm[i].firstElementChild.setAttribute("value", "true");
               divWithAwardForm[i].style.display = "none";
            });
        }
    }

    buttonAddNewAwardForm.addEventListener("click", function () {
        createNewAwardForm(document.getElementsByClassName("award-form").length, document.getElementById("add-award-div-form"));
    })
}

function createNewAwardForm(awardFormsSize, divWithAddButton) {
    const newAwardForm = addAwardFormToTheDOM(awardFormsSize, divWithAddButton);


    function addAwardFormToTheDOM(awardFormsSize, divWithAddButton) {
        const newForm = document.createElement("div");
        newForm.innerHTML = `
            <input type="text" name="awards[${awardFormsSize}].toDelete" value="false" hidden>
            <input type="text" name="awards[${awardFormsSize}].new" value="true" hidden>
            <div class="inline-block mt-2" id="date-select-div"></div>  :
            <input class="mx-2 w-60" placeholder="Наименование награды" name="awards[${awardFormsSize}].name" value="">
            <button class="button-delete-input btn btn-outline-danger" type="button">Удалить</button>
        `;
        newForm.classList.add("mb-2");
        newForm.classList.add("w-100");
        newForm.classList.add("award-form");
        newForm.classList.add("mt-2");
        const selectWithYears = createSelectWithDatesForAward(awardFormsSize);
        selectWithYears.classList.add("w-100");
        newForm.querySelector("div").append(selectWithYears);
        if (document.getElementById("award-form-empty") !== null){
            document.getElementById("award-form-empty").replaceWith(newForm);
            return newForm;
        }
        divWithAddButton.before(newForm);
        return newForm;
    }

    function createSelectWithDatesForAward(awardFormSize) {
        const select = document.createElement("select");
        select.classList.add("date-select");
        select.setAttribute("name", "awards[" + awardFormSize +  "].date");
        select.appendChild(createDefaultYearOption("Выберите дату"));
        for (let i = 1930; i < new Date().getFullYear(); i++) {
            let option = document.createElement("option");
            option.value = "" + i;
            option.text = "" + i;
            select.appendChild(option);
        }
        return select;
    }

    const deleteAwardFormButton = newAwardForm.lastElementChild;
    deleteAwardFormButton.addEventListener("click", function () {
            newAwardForm.firstElementChild.setAttribute("value", "true");
            newAwardForm.style.display = "none";
    });
}

function createNewFamilyForm(familyMembersFormsSize, divWithAddButton) {
    const newFamilyForm = addFamilyFormToTheDOM(familyMembersFormsSize, divWithAddButton);

    const deleteFormButton = newFamilyForm.lastElementChild.lastElementChild.firstElementChild;
    deleteFormButton.addEventListener("click", function () {
        newFamilyForm.firstElementChild.firstElementChild.setAttribute("value", "true");
        newFamilyForm.style.display = "none";
    });

    function addFamilyFormToTheDOM(familyMembersFormsSize, divWithAddButton) {
        const newForm = document.createElement("div");
        newForm.innerHTML = `
            <div class="mb-2 mt-2">
                <input hidden type="text" name="family[${familyMembersFormsSize}].toDelete" value="false">
                <input hidden type="text" name="family[${familyMembersFormsSize}].new" value="true">
                <input class="input-name" type="text" name="family[${familyMembersFormsSize}].relation" placeholder="Член семьи">:
                <input class="input-name" type="text" name="family[${familyMembersFormsSize}].surname" placeholder="Фамилия">
                <input class="input-name" type="text" name="family[${familyMembersFormsSize}].name" placeholder="Имя">
                <input class="input-name" type="text" name="family[${familyMembersFormsSize}].patronymic" placeholder="Отчество">
            </div>
            <div class="mb-2 justify-content-between d-flex block-w-delete">
                <div>
                    Дата рождения:
                    <input class="date-input" type="date" name="family[${familyMembersFormsSize}].birthDate">
                </div>
                <div>
                    <button class="btn btn-outline-danger button-delete-input mx-2" type="button">Удалить</button>
                </div>
            </div>
        `;
        newForm.classList.add("family-member-block");
        newForm.classList.add("block-medium-text");
        if (document.getElementById("family-member-block-empty") !== null){
            document.getElementById("family-member-block-empty").replaceWith(newForm);
        }
        else{
            divWithAddButton.before(newForm);
        }
        return newForm;
    }
}

function createNewForm(button, educationFormsSize) {
    const newForm = addFormToTheDOM(button, educationFormsSize);
    const deleteButton = newForm.lastElementChild.firstElementChild;
    deleteButton.addEventListener("click", function () {
        newForm.firstElementChild.setAttribute("value", "true");
        newForm.style.display = "none";
    });
}

function addFormToTheDOM(button, educationFormsSize) {
    const newForm = document.createElement("div");
    newForm.innerHTML = `
        <input hidden type="text" name="educations[${educationFormsSize}].toDelete" value="false">
        <input hidden type="text" name="educations[${educationFormsSize}].new" value="true">
        <div class="mb-2">
            Образование:
            <select name="educations[${educationFormsSize}].educationType" class="py-1">
                <option value="HIGHER">Высшее</option>
                <option value="SECONDARY">Среднее</option>
                <option value="PROFESSIONAL">Профессиональное</option>
                <option value="ADDITIONAL">Дополнительное</option>
            </select>
        </div>
        <div class="mb-2">
            <div class="inline-block">
                <p class="text-on-the-date">Выберите дату начала/окончания обучения</p>
                 <span> - </span> 
            </div>:
            <div class="inline-block w-35">
                <input class="w-100" type="text" name="educations[${educationFormsSize}].institution" value="" placeholder="Название учебного заведения">
            </div>
        </div>
        <div class="mb-2">
            Специальность:
            <input type="text" name="educations[${educationFormsSize}].speciality" value="" placeholder="Специальность">
        </div>
        <div class="mb-2">
            Квалификация:
            <select name="educations[${educationFormsSize}].diplomaType" class="py-1">
                <option value="NOT_SPECIFIED">Отсутствует</option>
                <option value="BACHELOR">Бакалавр</option>
                <option value="MASTER">Магистр</option>
                <option value="FELLOWSHIP">Ординатура</option>
                <option value="INTERNSHIP">Интернатура</option>
            </select>
        </div>
        <div class="button-delete-edu-form">
            <button type="button" class="btn btn-outline-danger button-add-new-form button-delete">Удалить</button>
        </div>
    `;

    newForm.classList.add("education-form");
    newForm.classList.add("edu-info");
    newForm.classList.add("text-line");
    newForm.setAttribute("id", "education-form" + educationFormsSize);
    newForm.children[3].firstElementChild.firstElementChild.after(createSelectWithDates(educationFormsSize, "Дата начала", "startEducation"));
    newForm.children[3].firstElementChild.append(createSelectWithDates(educationFormsSize, "Дата окончания", "endEducation"));
    if (document.getElementById("edu-form-empty") !== null){
        document.getElementById("edu-form-empty").replaceWith(newForm);
    }
    else{
        button.before(newForm);
    }


    function createSelectWithDates(educationFormsSize, defaultText, fieldName) {
        const select = document.createElement("select");
        select.classList.add("date-select");
        select.setAttribute("name", "educations[" + educationFormsSize +  "]." + fieldName);
        select.appendChild(createDefaultYearOption(defaultText));
        for (let i = 1930; i < new Date().getFullYear(); i++) {
            let option = document.createElement("option");
            option.value = "" + i;
            option.text = "" + i;
            select.appendChild(option);
        }
        return select;
    }

    return newForm;
}

function createNewDegreeInput(degreesInputsSize) {
    const newInput = addDegreeInputToTheDOM(degreesInputsSize);
    newInput.lastElementChild.addEventListener("click", function () {
        newInput.firstElementChild.setAttribute("value", "true");
        newInput.style.display = "none";
    });

    function addDegreeInputToTheDOM(degreesInputsSize) {
        const divWithInput = document.createElement("div");
        divWithInput.innerHTML = `
        <input hidden type="text" name="scienceDegree[${degreesInputsSize}].toDelete" value="false">
        <input hidden type="text" name="scienceDegree[${degreesInputsSize}].new" value="true">
        <input class="w-60" type="text" placeholder="Наименование" name="scienceDegree[${degreesInputsSize}].name" value="">
        <button class="ml-4 btn btn-outline-danger button-delete-input" type="button">Удалить</button>
        `;
        divWithInput.classList.add("mb-2");
        divWithInput.classList.add("degree-input");
        if (document.getElementById("degree-input-empty") !== null){
            document.getElementById("degree-input-empty").replaceWith(divWithInput);
        }
        else {
            document.getElementById("degrees-inputs").append(divWithInput);
        }

        return divWithInput;
    }
}

function createDefaultYearOption(defaultText) {
    const defaultOption = document.createElement("option");
    defaultOption.value = "" + new Date().getFullYear();
    defaultOption.text = defaultText;
    defaultOption.selected;
    return defaultOption;
}


function addFeaturesWithEmploymentsList() {
    if (document.getElementsByClassName("employment-block").length !== 0){
        const employmentRows = document.getElementsByClassName("employment-block");
        const lastEmployment = employmentRows[employmentRows.length - 1];
        lastEmployment.lastElementChild.firstElementChild.addEventListener("click", function () {
            lastEmployment.firstElementChild.setAttribute("value", "true");
            lastEmployment.style.display = "none";
            lastEmployment.classList.remove("d-flex");
        });

    }
}

function addFeaturesWithEmployeeActions() {
    if (document.getElementsByClassName("action-info-block").length !== 0){
        const actionsBlocks = document.getElementsByClassName("action-info-block");
        for (let i = 0; i < actionsBlocks.length; i++) {
            actionsBlocks[i].lastElementChild.firstElementChild.addEventListener("click", function () {
                actionsBlocks[i].firstElementChild.setAttribute("value", "true");
                actionsBlocks[i].style.display = "none";
                actionsBlocks[i].classList.remove("d-flex");
            });
        }
    }
}


