package com.ihsm.esdp.ideas.entities;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.information.Employee;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;



@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Getter
@Setter
@Table(name = "idea_answer")
public class AnswerIdea extends BaseEntity {

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String answer;

    @ManyToOne
    private Idea idea;

    @ManyToOne
    private Employee employee;

    @ManyToOne
    private User user;
}
