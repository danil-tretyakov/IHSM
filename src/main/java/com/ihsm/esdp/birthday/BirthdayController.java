package com.ihsm.esdp.birthday;


import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class BirthdayController {
    private final BirthdayService birthdayService;


    @GetMapping("/birthday")
    public String getEmpBirthday(Model model){
        model.addAttribute("employee", birthdayService.findAll());
        return "birthday/birthday";
    }

}
