package com.ihsm.esdp.users.controllers;

import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.security.MySecurityUser;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user/auth")
public class UserRestController {

    @GetMapping(path = "/", produces = MediaType.TEXT_PLAIN_VALUE)
    public String getUser(Authentication authentication){
        User user = ((MySecurityUser) authentication.getPrincipal()).getUser();
        String patronymicIfExist = "";
        if (user.getEmployee().getPatronymic() != null) patronymicIfExist = user.getEmployee().getPatronymic();
        return  user.getUserRole().getName() + ";" + user.getSurname() + ";" + user.getName() + ";" + patronymicIfExist;
    }
}
