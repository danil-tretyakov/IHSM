package com.ihsm.esdp.users.controllers;

import com.ihsm.esdp.documents.dtos.DocsDto;
import com.ihsm.esdp.documents.entities.Document;
import com.ihsm.esdp.documents.services.DocumentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/guide")
@RequiredArgsConstructor
public class GuideController {

    private final DocumentService documentService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    String getGuide(Model model) {
        List<DocsDto> docsForGuide = this.documentService.getAll().stream()
                .filter(document -> document.getDocumentName().contains("for_dismissal") || document.getDocumentName().contains("for_vacation") || document.getDocumentName().contains("for_vac_without_pay"))
                .map(document -> modelMapper.map(document, DocsDto.class))
                .collect(Collectors.toList());
        model.addAttribute("docs", docsForGuide);
        return "guide/action-guide";
    }

    @PreAuthorize(value = "hasAnyRole('ROLE_SUPER_ADMIN')")
    @PostMapping("/add-doc")
    public String addDoc(@RequestParam MultipartFile file, @RequestParam String doc_type) throws IOException {
        documentService.createDocForGuidePage(file, doc_type);
        return "redirect:/guide";
    }

    @PreAuthorize(value = "hasAnyRole('ROLE_SUPER_ADMIN')")
    @PostMapping("/delete/{id}")
    public String deleteDoc(@PathVariable Long id){
        documentService.deleteDoc(id);
        return "redirect:/guide";
    }
}
