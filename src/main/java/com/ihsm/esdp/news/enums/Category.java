package com.ihsm.esdp.news.enums;

public enum Category {
    Daily("Обычная"), OurLife("Наша жизнь"), PhotoGallery("Фотогаллерея");

    private final String name;

    Category(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
