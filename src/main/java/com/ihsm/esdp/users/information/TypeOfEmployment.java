package com.ihsm.esdp.users.information;


public enum TypeOfEmployment{
    NOT_SPECIFIED("Не указан"), MAIN("Основной"), COMBINATION("Совместитель");

    private final String name;

    TypeOfEmployment(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
