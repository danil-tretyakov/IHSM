package com.ihsm.esdp.photoGallery.gallery;

import com.ihsm.esdp.exceptions.NotFoundException;
import com.ihsm.esdp.news.entities.News;
import com.ihsm.esdp.news.enums.Category;
import com.ihsm.esdp.news.repositories.NewsRepository;
import com.ihsm.esdp.photoGallery.photo.Photo;
import com.ihsm.esdp.photoGallery.photo.PhotoRepository;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.enums.CategoryOfNotification;
import com.ihsm.esdp.users.service.EmailSenderService;
import com.ihsm.esdp.utility.MaltyPartFile;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AlbumService {

    private final PhotoRepository photoRepository;

    private final AlbumRepository albumRepository;

    private final NewsRepository newsRepository;

    private final EmailSenderService emailSenderService;

    private final MaltyPartFile maltyPartFile;

    private final String uploadPath = "upload/photo";

    public void creatAlbum(String galleryName, MultipartFile file, User user) throws IOException {
        Optional<Album> album = albumRepository.findByAlbumName(galleryName);
        LocalDateTime ldt = LocalDateTime.now();
        if (album.isEmpty()){
            Album newAlbum = new Album();
            newAlbum.setAlbumName(galleryName);
            newAlbum.setPhotoPath(maltyPartFile.uploadFile(uploadPath, file));
            Long savedAlbumId = albumRepository.save(newAlbum).getId();

            News newPost = News.builder()
                    .date(ldt)
                    .title(newAlbum.getAlbumName())
                    .text("Создан альбом \"" + newAlbum.getAlbumName() + "\"")
                    .user(user)
                    .imagePath(newAlbum.getPhotoPath())
                    .category(Category.PhotoGallery)
                    .build();
            this.newsRepository.save(newPost);

            this.emailSenderService.sendNotificationToAllUsers(galleryName, CategoryOfNotification.PHOTOGALLERY, savedAlbumId, "");
        }
    }

    public void removeAlbum(Long id) {
        Album album= albumRepository.findById(id).orElseThrow(NotFoundException::new);
        List<Photo> photoList = photoRepository.findByAlbum(album);
        for(Photo photo:photoList){
            photoRepository.delete(photo);
        }
        albumRepository.delete(album);
    }

    public Page<Album> getAllGallery(Pageable pageable) {
        return albumRepository.findAll(pageable);
    }

    public Page<Photo> getPhotosByAlbum(Long id,Pageable pageable) {
        Album album = albumRepository.findById(id).orElseThrow(NotFoundException::new);
        return photoRepository.findByAlbum(album,pageable);
    }

    public Album getAlbum(Long albumId) {
      return albumRepository.findById(albumId).orElseThrow(NotFoundException::new);
    }

    public Optional<Album> getById(Long id) {
        return this.albumRepository.findById(id);
    }
}