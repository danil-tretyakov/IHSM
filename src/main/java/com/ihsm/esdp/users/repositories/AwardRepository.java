package com.ihsm.esdp.users.repositories;

import com.ihsm.esdp.users.information.Award;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AwardRepository extends JpaRepository<Award, Long> {
}
