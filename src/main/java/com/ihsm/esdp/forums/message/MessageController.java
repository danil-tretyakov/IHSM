package com.ihsm.esdp.forums.message;

import com.ihsm.esdp.forums.topic.Topic;
import com.ihsm.esdp.forums.topic.TopicService;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.service.UserService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
@Transactional
@RequestMapping("/forum")
public class MessageController {
    private final TopicService topicService;
    private final MessageService messageService;
    private final ModelMapper mapper= new ModelMapper();
    private final UserService userService;

    @GetMapping("/questionnaire/{id}")
    public String displayTopicsInForum(@PathVariable Long id, Model model){
        Topic topic=topicService.getTopic(id);
        List<Message> messages = messageService.getTopicById(id);
        model.addAttribute("topic",topic);
        model.addAttribute("message", messages.stream().map(p -> mapper.map(p, Message.class)).distinct().sorted(Comparator.comparing(Message::getLocalDateTime, Comparator.nullsLast(Comparator.reverseOrder()))).collect(Collectors.toList()));
        return "forum/messageTopic";
    }

    @PostMapping("/add/{id}")
    public String  addAnswer(@PathVariable Long id, Message answerResponse, Principal principal){
        messageService.saveMassage(id,answerResponse,principal);
        return "redirect:/forum/questionnaire/{id}";
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String deleteAnswer(@PathVariable Long id,@RequestParam Long topicId){
        messageService.deleteMessage(id);
        return "redirect:/forum/questionnaire/" + topicId;
    }

    @PostMapping("/block/user/{id}")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String block(@PathVariable Long id){
        User user = this.userService.getById(id);
        user.setBlocked(true);
        userService.saveUser(user);
        messageService.deleteAllMessage(id);
        return "redirect:/forum";
    }

    @PostMapping("/unblock/user/{id}")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String unblock(@PathVariable Long id){
        User user = this.userService.getById(id);
        user.setBlocked(false);
        userService.saveUser(user);
        return "redirect:/forum";
    }
}
