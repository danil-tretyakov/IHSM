package com.ihsm.esdp.users.repositories;

import com.ihsm.esdp.users.information.Employment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

public interface EmploymentRepository extends JpaRepository<Employment, Long> {

    Optional<Employment> findByTransferDateOutAndEmployee_Id(LocalDate date, Long id);

    @Transactional
    @Modifying
    @Query("update Employment e set e.transferDateOut = null where e.id = ?1")
    void setNewDateOutOfEmployment(Long id);
}
