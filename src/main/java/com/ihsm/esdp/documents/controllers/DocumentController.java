package com.ihsm.esdp.documents.controllers;


import com.ihsm.esdp.documents.categories.CategoryDocs;
import com.ihsm.esdp.documents.services.DocumentService;
import com.ihsm.esdp.documents.categories.CategoryService;
import com.ihsm.esdp.documents.entities.Document;
import com.ihsm.esdp.news.enums.Category;
import com.ihsm.esdp.utility.PaginationService;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@Controller
@RequestMapping("/documents")

public class DocumentController {

    private final ModelMapper modelMapper = new ModelMapper();

    private final DocumentService documentService;

    private final CategoryService categoryService;

    private final PaginationService paginationService;

    @GetMapping("/category/{categoryDocsId}")
    public String showDocumentsByCategory(@PathVariable Long categoryDocsId, Model model, @PageableDefault Pageable pageable) {
        Optional<CategoryDocs> categoryDocsById = this.categoryService.getCategoryById(categoryDocsId);
        if (categoryDocsById.isEmpty()) return "redirect:/";
        Page<Document> docs = documentService.getDocsByCategories(categoryDocsId, pageable);
        model.addAttribute("docs", docs
                .stream()
                .map(document -> modelMapper.map(document, Document.class))
                .sorted(Comparator.comparing(Document::getFileDate, Comparator.nullsLast(Comparator.reverseOrder())))
                .collect(Collectors.toList()));
        model.addAttribute("categoryDocsObj", categoryDocsById.get());
        this.paginationService.setPaginationVariablesInModel(model, docs.getPageable(), docs.getTotalPages());
        return "docs/documents";
    }
}