package com.ihsm.esdp.forums.topic;

import com.ihsm.esdp.forums.message.Message;
import com.ihsm.esdp.forums.message.MessageService;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.service.UserService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class TopicController {
    private final TopicService topicService;
    private final MessageService messageService;
    private final UserService userService;
    private final ModelMapper mapper= new ModelMapper();

    @GetMapping("/forum")
    public String displayAllTopic(Model model, Principal principal) {
        List<Topic> allTopics = topicService.getAll();
        model.addAttribute("topics", allTopics.stream().map(p -> mapper.map(p, Topic.class)).distinct().sorted(Comparator.comparing(Topic::getLocalDateTime, Comparator.nullsLast(Comparator.reverseOrder()))).collect(Collectors.toList()));
        model.addAttribute("user", principal);
        User user = userService.findByEmail(principal.getName());
        if (user.isBlocked()){ return "forum/noAccess"; }
        return "forum/forumTopic";
    }

    @PostMapping("/add")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String addTopic(Topic topic,Principal principal){
        topicService.addTopic(topic,principal);
        return "redirect:/forum";
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String deleteTopic(@PathVariable(value = "id") Long id){
        Topic topic = topicService.getTopic(id);
        List<Message> message = messageService.getByTopic(topic.getId());
        for (Message m: message) { messageService.deleteMessage(m.getId()); }
        topicService.deleteTopic(id);
        return "redirect:/forum";
    }

    @GetMapping("/blockUsers")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String blockUsers(Model model){
        List<User> user = userService.blockUser();
        model.addAttribute("user", user);
        return "forum/blockUsers";
    }
}
