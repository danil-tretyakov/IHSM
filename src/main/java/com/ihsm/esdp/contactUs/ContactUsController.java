package com.ihsm.esdp.contactUs;

import com.ihsm.esdp.contactUs.dto.NewMessageDto;
import com.ihsm.esdp.users.security.MySecurityUser;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
public class ContactUsController {

    private final ContactUsMailSenderService contactUsMailSenderService;

    @GetMapping("/contactUs")
    public String contactUsPage(Model model, Authentication authentication) {
        MySecurityUser mySecurityUser = (MySecurityUser) authentication.getPrincipal();
        model.addAttribute("email", mySecurityUser.getUser().getEmail());
        model.addAttribute("message", new NewMessageDto());
        return "contactus/contactUs";
    }

    @GetMapping("/contactUs/confirm")
    public String showConfirmPage(Model model, Authentication authentication){
        MySecurityUser mySecurityUser = (MySecurityUser) authentication.getPrincipal();
        model.addAttribute("email", mySecurityUser.getUser().getEmail());
        return "contactus/confirm";
    }

    @PostMapping("/sendEmail")
    public String sendEmailFromContactUsForm(@Valid @ModelAttribute(name ="newMessage") NewMessageDto messageDTO, BindingResult bindingResult,
                                             RedirectAttributes attributes) {
        attributes.addFlashAttribute("message", messageDTO);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors",bindingResult.getFieldErrors());
            return "contactus/contactUs";
        }
        contactUsMailSenderService.sendContactUsMail(messageDTO);
        return "redirect:/contactUs/confirm";
    }

}
