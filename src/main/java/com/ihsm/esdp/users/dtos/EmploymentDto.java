package com.ihsm.esdp.users.dtos;

import com.ihsm.esdp.users.information.Employment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentDto extends Employment {
    private boolean toDelete;
}
