package com.ihsm.esdp.ideas.dtos;


import lombok.*;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode()
public class AnswerIdeaDTO {

    private String answer;

    private Long idea_id;

    private Long user_id;
}
