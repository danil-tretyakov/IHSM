package com.ihsm.esdp.users.dtos.employeeActionsDtos.postDtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VacationPostDto {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startVacation;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endVacation;

    private long employeeId;
}
