'use strict'

function loadFile(event) {
    const image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
    image.classList.add("height-auto");
    if (document.contains(document.getElementsByClassName("cam-on-center")[0])){
        const camera = document.getElementsByClassName("cam-on-center")[0];
        camera.remove();
    }
}
