package com.ihsm.esdp.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Value("photo/")
    private String pathToPhotoThemePhotos;

    @Value("users/")
    private String pathToUsersPhotos;

    @Value("news/")
    private String pathToNewsPhotos;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/img/users/default/default.jpeg")
                .addResourceLocations("file:src/main/resources/static/img/default.jpeg");
        registry.addResourceHandler("/img/news/default/default.jpeg")
                .addResourceLocations("file:src/main/resources/static/img/default.jpeg");
        registry.addResourceHandler("/img/photo/**")
                .addResourceLocations("file:upload/" + pathToPhotoThemePhotos);
        registry.addResourceHandler("/img/users/**")
                .addResourceLocations("file:upload/" + pathToUsersPhotos);
        registry.addResourceHandler("/img/news/**")
                .addResourceLocations("file:upload/" + pathToNewsPhotos);
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
    }
}
