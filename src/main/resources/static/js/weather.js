function weatherBalloon( cityID ) {
    let key = 'b41426b47a90d603a4b0bcf27c707d57';
    fetch('https://api.openweathermap.org/data/2.5/weather?id=' + cityID+ '&appid=' + key)
        .then(function(resp) { return resp.json() }) // Convert data to json
        .then(function(data) {
            drawWeather(data);
            // alert(data)
        })
        .catch(function() {
            // catch any errors
        });
}

window.onload = function() {
    getUser();
    initButtonDeps();
    initButtonDocs();
    weatherBalloon( 1528675);
}
function drawWeather( d ) {
    let celcius = Math.round(parseFloat(d.main.temp)-273.15);
    let fahrenheit = Math.round(((parseFloat(d.main.temp)-273.15)*1.8)+32);
    let description = d.weather[0].description;

    document.getElementById('description').innerHTML = description;
    document.getElementById('temp').innerHTML = celcius + '&deg;';
    document.getElementById('location').innerHTML = d.name;

    if( description.indexOf('rain') > 0 ) {
        document.body.className = 'rainy';
    } else if( description.indexOf('cloud') > 0 ) {
        document.body.className = 'cloudy';
    } else if( description.indexOf('sunny') > 0 ) {
        document.body.className = 'sunny';
    }
}