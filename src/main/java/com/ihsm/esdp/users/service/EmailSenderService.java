package com.ihsm.esdp.users.service;

import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.UserRole;
import com.ihsm.esdp.users.enums.CategoryOfNotification;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmailSenderService {

    private final JavaMailSender javaMailSender;

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Async
    public void sendEmail(SimpleMailMessage email){
        javaMailSender.send(email);
    }

    public void sendConfirmationPasswordMail(String userMail,String password) {

        final SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(userMail);
        mailMessage.setSubject("Your personal data to sing in on internal site");
        mailMessage.setFrom("<MAIL>");
        mailMessage.setText(
                "Email: "+userMail
                        +"\nPassword: "+password
        );

        sendEmail(mailMessage);
    }

    @Async
    public void sendNotificationToAllUsers(String nameOfEntry, CategoryOfNotification categoryOfNotification, Long idOfEntry, String deparmentName) {
        List<User> listOfAllUsers = this.userService.getAllUsers();
        listOfAllUsers.removeIf(user -> UserRole.ROLE_SUPER_ADMIN.equals(user.getUserRole()));
            switch (categoryOfNotification){
                case NEWS:
                    createAndSendNotifications(nameOfEntry, listOfAllUsers, "Новости", "появилась новая информация", "192.168.8.51/news/" + idOfEntry, "Новая запись");
                    break;
                case DOCUMENT:
                    createAndSendNotifications(nameOfEntry, listOfAllUsers, "Все подразделения\"(" + deparmentName + ") и \"Документы", "размещен новый документ", "192.168.8.51/departments/department/" + idOfEntry, "Новый документ");
                    break;
                case PHOTOGALLERY:
                    createAndSendNotifications(nameOfEntry, listOfAllUsers, "Фото галерея", "появился новый альбом", "192.168.8.51/gallery/album/" + idOfEntry, "Новый фотоальбом");
                    break;
            }
    }

    private void createAndSendNotifications(String nameOfEntry, List<User> listOfAllUsers, String nameOfSection, String descriptionOfEntry, String linkToEntry, String subject){
        for (User userFromList : listOfAllUsers) {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(userFromList.getEmail());
            mailMessage.setSubject(subject + " на внутреннем сайте");
            mailMessage.setFrom("<MAIL>");
            mailMessage.setText(String.format("Уважаемые коллеги, на внутреннем сайте (board) в разделе/разделах \"%s\" %s \"%s\".\nСсылка: %s", nameOfSection, descriptionOfEntry, nameOfEntry, linkToEntry));

            sendEmail(mailMessage);
        }

    }

}
