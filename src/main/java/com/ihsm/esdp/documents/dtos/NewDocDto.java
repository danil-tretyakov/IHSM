package com.ihsm.esdp.documents.dtos;

import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.documents.categories.CategoryDocs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewDocDto {

    @NotNull
    private CategoryDocs categoryDocs;

    private Department department;

    private MultipartFile file;

}
