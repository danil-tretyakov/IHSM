package com.ihsm.esdp.forums.message;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.forums.topic.Topic;
import com.ihsm.esdp.users.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "messages")
public class Message extends BaseEntity {

    private String idea;

    @ManyToOne(fetch = FetchType.LAZY)
    private Topic topic;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @DateTimeFormat(pattern ="dd/mm/yy")
    private LocalDateTime localDateTime;

    public Message(String idea, Topic topic, User user, LocalDateTime localDateTime) {
        this.idea = idea;
        this.topic = topic;
        this.user = user;
        this.localDateTime = localDateTime;
    }
}
