package com.ihsm.esdp.utility;

import com.ihsm.esdp.news.entities.News;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class PaginationService {

    public void setPaginationVariablesInModel(Model model, Pageable pageable, int totalPages){
//        model.addAttribute("news",newsWithPagination.getContent().stream()
//                .map(news -> modelMapper.map(news, NewsDto.class))
//                .collect(Collectors.toList()));
        model.addAttribute("pages", pageable);
        model.addAttribute("allPages", totalPages);
        if (pageable.getPageNumber() > 9){
            model.addAttribute("firstPageOfSequence",  pageable.getPageNumber() - pageable.getPageNumber()%10);
            if (totalPages - (pageable.getPageNumber() + 1) < 10){
                model.addAttribute("totalPage", totalPages);
                return;
            }
            model.addAttribute("totalPage", (pageable.getPageNumber() - pageable.getPageNumber()%10) + 10);
            return;
        }
        model.addAttribute("firstPageOfSequence", 0);
        if(totalPages > 10){
            model.addAttribute("totalPage", 10);
            return;
        }
        model.addAttribute("totalPage", totalPages);
    }
}
