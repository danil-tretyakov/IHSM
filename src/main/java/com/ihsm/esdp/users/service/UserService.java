package com.ihsm.esdp.users.service;

import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.departments.DepartmentService;
import com.ihsm.esdp.exceptions.EmployeeNotFoundException;
import com.ihsm.esdp.exceptions.NotFoundException;
import com.ihsm.esdp.users.dtos.contacts.ContactUpdateDto;
import com.ihsm.esdp.users.information.Employee;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.exceptions.UserAlreadyRegisteredException;
import com.ihsm.esdp.users.information.EmployeePosition;
import com.ihsm.esdp.users.repositories.EmployeePositionRepository;
import com.ihsm.esdp.users.repositories.EmployeeRepository;
import com.ihsm.esdp.users.repositories.UserRepository;
import com.ihsm.esdp.utility.MaltyPartFile;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final EmployeeRepository employeeRepository;

    private final EmployeePositionRepository employeePositionRepository;

    private final PasswordEncoder bCryptPasswordEncoder;

    private EmailSenderService emailSenderService;

    private final DepartmentService departmentService;

    private final MaltyPartFile maltyPartFile;

    @Autowired
    public void setEmailSenderService(@Lazy EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    public Optional<User> findUserForEmployeeDataById(Long id){
        return this.userRepository.findById(id);
    }

    public Optional<Employee> findEmployeeId(Long employeeId) {
        return this.employeeRepository.findById(employeeId);
    }


    public User signUpUser(User user) throws UserAlreadyRegisteredException{
        if (userRepository.findByEmail(user.getEmail()).isPresent()){
            throw new UserAlreadyRegisteredException();
        }

        final String password = RandomStringUtils.random(8,true,true);
        final String encryptedPassword = bCryptPasswordEncoder.encode(password);

        user.setPassword(encryptedPassword);
        user.setEmail(user.getEmail().toLowerCase());
        User userWithId = userRepository.save(user);

        Employee employee = new Employee();

        user.setEmployee(employee);

        employee.setUser(user);
        employeeRepository.save(employee);

        this.emailSenderService.sendConfirmationPasswordMail(user.getEmail(), password);
        return userWithId;
    }

    public List<Department> getAllDepartments() {
        return this.departmentService.getAll();
    }

    public List<Employee> getAllEmployees() {
        return this.employeeRepository.findAll();
    }
    public List<Employee> findEmployeesByParameter(String param) {
        return this.employeeRepository.findAllByUser_NameContainingOrUser_SurnameContainingOrPatronymicContainingOrSubject_NameContaining(param, param, param, param);
    }

    public void updateNewEmployeeInfo(ContactUpdateDto employeeWithNewInfo, Long employeeId) throws EmployeeNotFoundException {
        Employee employeeToUpdate = this.employeeRepository.findById(employeeId).orElseThrow(EmployeeNotFoundException::new);
        if (!employeeWithNewInfo.getImage().isEmpty()) {
            try{
                employeeToUpdate.setImagePath(maltyPartFile.uploadFile("upload/users", employeeWithNewInfo.getImage()));
            }catch (IOException ex){
                employeeToUpdate.setImagePath(employeeWithNewInfo.getImagePath());
            }
        }

        employeeToUpdate.getUser().setName(employeeWithNewInfo.getUser().getName());
        employeeToUpdate.getUser().setSurname(employeeWithNewInfo.getUser().getSurname());
        employeeToUpdate.getUser().setEmail(employeeWithNewInfo.getUser().getEmail());
        employeeToUpdate.setPatronymic(employeeWithNewInfo.getPatronymic());
        employeeToUpdate.setDepartment(employeeWithNewInfo.getDepartment());
        employeeToUpdate.setPhoneNumber(employeeWithNewInfo.getPhoneNumber());
        employeeToUpdate.setTypeOfEmployment(employeeWithNewInfo.getTypeOfEmployment());
        if (employeeWithNewInfo.getEmployeePosition() != null) employeeToUpdate.setEmployeePosition(checkEmployeePositionIfExist(employeeWithNewInfo.getEmployeePosition()));
        this.employeeRepository.save(employeeToUpdate);
    }

    private EmployeePosition checkEmployeePositionIfExist(EmployeePosition employeePosition) {
        return this.employeePositionRepository.findByName(employeePosition.getName()).orElse(employeePosition);
    }

    public Optional<User> findUserByMail(String mail) {
        return userRepository.findByEmail(mail);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(NotFoundException::new);
    }

    public User getById(Long id){
        return userRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public void saveUser(User user){
        userRepository.save(user);
    }

    public List<User> blockUser(){
        return userRepository.findByBlockedIsTrue();
    }

    public List<User> getAllUsers() {
        return this.userRepository.findAll();
    }
}


