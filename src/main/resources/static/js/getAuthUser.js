function getUser() {
    const settings ={
        mode: 'no-cors',
        method: 'GET'
    };

    fetch("http://192.168.8.51/user/auth/", settings).then(function (response) {return response.text()})
        .then(function (text) {
            if (text !== ""){
                const fullName = text.split(";");
                addUserToDOM(fullName[0], fullName[1], fullName[2], fullName[3]);
                return;
            }
            fetch("http://192.168.8.51:8081/user/auth/", settings).then(function (response) {return response.text()})
                .then(function (text) {
                    const fullName = text.split(";");
                    addUserToDOM(fullName[0], fullName[1], fullName[2], fullName[3]);
                });
        });


    function addUserToDOM(role, surname, name, patronymic) {
        const authBlock = document.getElementById("nav-auth");
        const newText = document.createElement("p");

        newText.innerHTML = `${surname} ${name} ${patronymic} (${role})`;
        newText.classList.add("mb-0");
        authBlock.append(newText);
    }
}
