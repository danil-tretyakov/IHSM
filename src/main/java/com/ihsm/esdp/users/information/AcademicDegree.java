package com.ihsm.esdp.users.information;

public enum AcademicDegree {
    NOT_SPECIFIED("Отсутствует"), BACHELOR("Бакалавр"), MASTER("Магистр"), FELLOWSHIP("Ординатура"), INTERNSHIP("Интернатура");

    private final String name;

    AcademicDegree(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
