package com.ihsm.esdp.birthday;

import com.ihsm.esdp.users.information.Employee;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BirthdayService {

    private final BirthdayRepository birthDayRepository;

    public List<Employee> findAll(){
        LocalDate ld = LocalDate.now();
        return birthDayRepository.findByBirthDate(ld.getMonthValue(), ld.getDayOfMonth());
    }

}
