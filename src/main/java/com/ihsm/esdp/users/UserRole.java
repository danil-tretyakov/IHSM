package com.ihsm.esdp.users;

public enum UserRole {
    ROLE_USER("Пользователь"),ROLE_ADMIN("Администратор"),ROLE_ADMIN_HR("Администратор ОУП"),ROLE_SUPER_ADMIN("Суперадмин");

    private final String name;

    UserRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
