package com.ihsm.esdp.departments;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "departments")
public class Department extends BaseEntity {

    @Column
    private String name;

}
