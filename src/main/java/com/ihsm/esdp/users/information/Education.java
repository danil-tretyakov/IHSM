package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.*;

import javax.persistence.*;


@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "educations")
public class Education extends BaseEntity {

    @Column
    private String institution;

    @Column
    private String speciality;

    @Column
    private int startEducation;

    @Column
    private int endEducation;

    @Enumerated(EnumType.STRING)
    @Column
    private AcademicDegree diplomaType;

    @Enumerated(EnumType.STRING)
    @Column
    private EducationType educationType;

    @ManyToOne
    private Employee employee;
}
