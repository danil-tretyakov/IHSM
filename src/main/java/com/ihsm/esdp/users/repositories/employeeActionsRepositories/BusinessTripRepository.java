package com.ihsm.esdp.users.repositories.employeeActionsRepositories;

import com.ihsm.esdp.users.information.BusinessTrip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessTripRepository extends JpaRepository<BusinessTrip, Long> {
}
