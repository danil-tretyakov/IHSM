package com.ihsm.esdp.photoGallery.photo;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.photoGallery.gallery.Album;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "photos")
public class Photo extends BaseEntity {

    private String photoNamePath;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(
            name = "album_id",
            referencedColumnName = "id"
    )
    private Album album;

    public Photo() {
    }

    public Photo(String photoNamePath, Album album) {
        this.photoNamePath = photoNamePath;
        this.album = album;
    }

    public String getPhotoNamePath() {
        return photoNamePath;
    }

    public void setPhotoNamePath(String photoNamePath) {
        this.photoNamePath = photoNamePath;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }
}
