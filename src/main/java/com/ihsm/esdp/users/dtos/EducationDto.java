package com.ihsm.esdp.users.dtos;

import com.ihsm.esdp.users.information.Education;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class EducationDto extends Education {
    private boolean toDelete;
    private boolean isNew;
}
