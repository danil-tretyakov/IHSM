package com.ihsm.esdp.forums.message;

import com.ihsm.esdp.exceptions.NotFoundException;
import com.ihsm.esdp.forums.topic.Topic;
import com.ihsm.esdp.forums.topic.TopicService;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;
    private final TopicService topicService;
    private final UserService userService;

    public void deleteMessage(Long id) {
        Message message = messageRepository.findById(id).orElseThrow(NotFoundException::new);
        messageRepository.delete(message);
    }

    public List<Message> getByTopic(Long topicId) {
        return messageRepository.findAllByTopicId(topicId);
    }

    public List<Message> getTopicById(Long id) {
        return messageRepository.findAllByTopicId(id);
    }

    public void saveMassage(Long id, Message answerResponse, Principal principal){
        User user = userService.findByEmail(principal.getName());
        Topic topic = topicService.getTopic(id);
        LocalDateTime localDateTime = LocalDateTime.now();
        Message answer = new Message(answerResponse.getIdea(),topic,user,localDateTime);
        messageRepository.save(answer);
    }
    public void deleteAllMessage(Long id) {
        messageRepository.deleteAllByUserId(id);
    }

}
