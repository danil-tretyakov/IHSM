package com.ihsm.esdp.ideas.dtos;

import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode()
public class ShowIdeaDto {
    private String name;
    private String surname;
    private String ideaName;
}
