package com.ihsm.esdp.departments;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/departments")
@RequiredArgsConstructor
public class DepartmentRestController {

    private final DepartmentService departmentService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DepartmentDto> displayAllDepartments(){

        return departmentService.getAll().stream()
                .map(department -> modelMapper.map(department, DepartmentDto.class))
                .filter(departmentDto -> departmentDto.getId() != 15)
                .collect(Collectors.toList());
    }
}
