package com.ihsm.esdp.users.repositories;

import com.ihsm.esdp.users.information.Employee;
import com.ihsm.esdp.users.information.EmployeePosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> findAllByUser_NameContainingOrUser_SurnameContainingOrPatronymicContainingOrSubject_NameContaining(String name, String surname, String middleName, String subjectName);
    List<Employee> findAllByUser_NameContainingOrUser_SurnameContainingOrPatronymicContaining(String name, String surname, String middleName);
    @Query(value = "select e.id,  e.user.surname, e.user.name, e.patronymic from employee e ")
    List<String> findAllByNames();


    @Transactional
    @Modifying
    @Query("update employee e set e.employeePosition = ?2 where e.id = ?1")
    void setNewEmployeePosition(Long id, EmployeePosition employeePosition);


}
