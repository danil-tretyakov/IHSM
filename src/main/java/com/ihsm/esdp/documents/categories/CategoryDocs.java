package com.ihsm.esdp.documents.categories;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "docs_categories")
public class CategoryDocs  extends BaseEntity {

    @Column(name = "category_name")
    private String categoryName;

}
