package com.ihsm.esdp.users;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.users.information.Employee;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User extends BaseEntity{

    @Size(min=3, max=24, message = "Length must be >= 3 and <= 24")
    @Pattern(regexp = "^[^\\d\\s]+$", message = "Should contain only letters")
    private String name;

    @Size(min=3, max=24, message = "Length must be >= 3 and <= 24")
    @Pattern(regexp = "^[^\\d\\s]+$", message = "Should contain only letters")
    private String surname;

    @NotBlank
    @Email
    private String email;

    @Column
    private String password;

    @OneToOne(mappedBy = "user")
    private Employee employee;

    @Enumerated(EnumType.STRING)
    @Column
    private UserRole userRole;

    @Column
    @Builder.Default
    private boolean blocked = false;

    @Builder.Default
    private Status status = Status.ACTIVE ;
}

