package com.ihsm.esdp.users.dtos.employeeActionsDtos.postDtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BusinessTripPostDto {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBeginning;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfEnding;

    private String purposeOfTheTrip;

    private long orderNumber;

    private long employeeId;
}
