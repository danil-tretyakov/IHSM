package com.ihsm.esdp.users.dtos.employeeActionsDtos.postDtos;

import com.ihsm.esdp.users.information.VacationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VacationOrderPostDto {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startVacation;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endVacation;

    private VacationType vacationType;

    private long orderNumber;

    private Long employeeId;
}
