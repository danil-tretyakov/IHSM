function initButtonDeps() {
    const buttonOfDepartments = document.getElementById("button-deps");
    buttonOfDepartments.addEventListener("click", function (event) {
        event.preventDefault();
        createListOfDeps();
    });
}

function createListOfDeps() {
    if (document.getElementsByClassName("deps-link").length === 0){

    const settings ={
        mode: 'no-cors',
        method: 'GET'
    };

    fetch("http://192.168.8.51/departments/all", settings).then(function (response) {return response.text()})
        .then(function (text) {
            try{
                const depsList = JSON.parse(text);
                addDeps(depsList);
            }
            catch (e) {
                fetch("http://192.168.8.51:8081/departments/all", settings).then(function (response) {return response.text()})
                    .then(function (text) {
                        const depsList = JSON.parse(text);
                        addDeps(depsList);
                    });
            }
        });
    }
}

function addDeps(depsList) {
    const depsLi = document.getElementById("departments");

    for (let i = 0; i < depsList.length; i++) {
        const newLink = document.createElement("div");
        newLink.innerHTML=`
        <a href="/departments/department/${depsList[i].id}" class="accordion-links text-dark">${depsList[i].name}</a>
        `;
        newLink.classList.add("deps-link");
        newLink.classList.add("w-100");
        depsLi.append(newLink);
    }
}

