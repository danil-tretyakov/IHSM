package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "vacation_orders")
public class VacationOrder extends BaseEntity {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private LocalDate startVacation;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private LocalDate endVacation;

    @Enumerated(EnumType.STRING)
    @Column
    private VacationType vacationType;

    @Column
    private long orderNumber;

    @ManyToOne
    private Employee employee;
}
