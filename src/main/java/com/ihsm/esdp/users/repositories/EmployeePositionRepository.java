package com.ihsm.esdp.users.repositories;

import com.ihsm.esdp.users.information.EmployeePosition;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmployeePositionRepository extends JpaRepository<EmployeePosition, Long> {
    Optional<EmployeePosition> findByName(String name);
}
