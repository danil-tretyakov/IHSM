package com.ihsm.esdp.departments;

import com.ihsm.esdp.documents.categories.CategoryService;
import com.ihsm.esdp.documents.services.DocumentService;
import com.ihsm.esdp.users.UserRole;
import com.ihsm.esdp.users.security.MySecurityUser;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping("/departments")
public class DepartmentController {

    private final DocumentService documentService;
    private final CategoryService categoryService;
    private final DepartmentService departmentService;

    @GetMapping("/department/{depId}")
    public String showDocsByDeps(Model model, @PathVariable Long depId, Authentication authentication){
        Optional<Department> departmentById = this.departmentService.getById(depId);
        if (departmentById.isEmpty()) return "redirect:/";
        MySecurityUser mySecurityUser = (MySecurityUser) authentication.getPrincipal();
        if ((mySecurityUser.getUser().getEmployee().getDepartment() != null && mySecurityUser.getUser().getEmployee().getDepartment().getId().equals(depId)) || UserRole.ROLE_SUPER_ADMIN.equals(mySecurityUser.getUser().getUserRole())){
            model.addAttribute("depUser", true);
        }
        else model.addAttribute("depUser", false);
        model.addAttribute("deps", documentService.getAllByDeps(depId));
        model.addAttribute("categories", categoryService.getCategory());
        model.addAttribute("departmentName", departmentById.get().getName());
        return  "deps/docs-by-deps";
    }
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    @PostMapping("/add-doc/{depId}")
    public String addDoc( @RequestParam Long id, @RequestParam MultipartFile file, @PathVariable Long depId) throws IOException {
        documentService.createDocs(file, id, depId);
        return "redirect:/departments/department/" +depId;
    }

    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    @PostMapping("/delete/{id}")
    public String deleteDoc(@PathVariable Long id, @RequestParam Long depId){
        documentService.deleteDoc(id);
        return "redirect:/departments/department/" + depId;
    }

}
