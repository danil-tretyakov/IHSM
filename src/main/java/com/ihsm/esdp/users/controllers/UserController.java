package com.ihsm.esdp.users.controllers;

import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.exceptions.UserAlreadyRegisteredException;
import com.ihsm.esdp.users.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;


@Controller
@RequestMapping("/")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/sign-in")
    String signIn() {
        return "login/sign-in";
    }

    @GetMapping("/error")
    public String errorPage() {
        return "error";
    }

    @GetMapping("/sign-up")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    String signUpPage(Model model) {
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new User());
        }
        return "login/sign-up";
    }

    @PostMapping("/sign-up")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    String signUp(Model model, @Valid @ModelAttribute("user") User user, BindingResult bindingResult) {

        model.addAttribute("user", user);
        if (bindingResult.hasFieldErrors()) {
            model.addAttribute("errors", bindingResult.getFieldErrors());
            return "login/sign-up";
        }
        User savedUser;
        try{
            savedUser = this.userService.signUpUser(user);
        }catch (UserAlreadyRegisteredException ex){
            model.addAttribute("emailExist", String.format("Пользователь с почтой %s уже существует", user.getEmail()));
            model.addAttribute("user", user);
            return "login/sign-up";
        }
        return "redirect:/employees/management/" + savedUser.getId();

    }
}

