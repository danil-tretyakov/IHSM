package com.ihsm.esdp.users.dtos.employeeActionsDtos.postDtos;

import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.users.information.EmployeePosition;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionPostDto {
    private long orderNumber;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfTransaction;

    private EmployeePosition employeePosition;

    private Department department;

    private Long employeeId;
}
