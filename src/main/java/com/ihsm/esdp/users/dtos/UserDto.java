package com.ihsm.esdp.users.dtos;

import lombok.Data;

@Data
public class UserDto {
    private String email;
}
