package com.ihsm.esdp.forums.topic;

import com.ihsm.esdp.exceptions.NotFoundException;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class TopicService {
    private final  TopicRepository topicRepository;
    private final UserService userService;

    public void addTopic(Topic topic, Principal principal) {
        User user = userService.findByEmail(principal.getName());
        LocalDateTime now = LocalDateTime.now();
        topic.setLocalDateTime(now);
        Topic topic1 = new Topic(user,topic.getName(),topic.getLocalDateTime());
        topicRepository.save(topic1);
    }

    public Topic getTopic(Long id) {
        return topicRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public void deleteTopic(Long id) {
        Topic topic2 = topicRepository.findById(id).orElseThrow(NotFoundException::new);
        topicRepository.delete(topic2);
    }

    public List<Topic> getAll() {
        return topicRepository.findAll();
    }
}
