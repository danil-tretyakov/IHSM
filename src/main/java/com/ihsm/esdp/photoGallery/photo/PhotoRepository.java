package com.ihsm.esdp.photoGallery.photo;

import com.ihsm.esdp.photoGallery.gallery.Album;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long> {
    Page<Photo>findByAlbum(Album album, Pageable pageable);
    List<Photo>findByAlbum(Album album);
    Optional<Photo>findById(Long id);

}
