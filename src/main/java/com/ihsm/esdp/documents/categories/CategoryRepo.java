package com.ihsm.esdp.documents.categories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CategoryRepo extends JpaRepository<CategoryDocs, Long> {
    List<CategoryDocs> findAllByCategoryNameIsContaining(String nameOfTopic);
}
