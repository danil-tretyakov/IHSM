package com.ihsm.esdp.users.information;

public enum MaritalStatus {
    NOT_SPECIFIED("Не указано"), MARRIED("В браке"), SINGLE("Холост");

    private final String name;

    MaritalStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
