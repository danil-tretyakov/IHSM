package com.ihsm.esdp.ideas.services;

import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.departments.DepartmentService;
import com.ihsm.esdp.exceptions.NotFoundException;

import com.ihsm.esdp.ideas.dtos.IdeaFormDto;
import com.ihsm.esdp.ideas.entities.Idea;

import com.ihsm.esdp.ideas.repository.IdeaRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@Service
@RequiredArgsConstructor
public class IdeaService {
    private final IdeaRepository ideaRepository;
    private final DepartmentService departmentService;

    public List<Department> getDepartments() {
        return departmentService.getAll();
    }

    public void addIdea(IdeaFormDto ideaFormDto){
        Department department = departmentService.getDepartmentByName(ideaFormDto.getDepartment().getName()).orElseThrow(NotFoundException::new);

        Idea idea = Idea.builder()
                .name(ideaFormDto.getName())
                .surname(ideaFormDto.getSurname())
                .position(ideaFormDto.getPosition())
                .department(String.valueOf(department.getName()))
                .ideaName(ideaFormDto.getIdeaName())
                .categoryIdea(ideaFormDto.getCategoryIdea())
                .text(ideaFormDto.getText())
                .resource(ideaFormDto.getResource())
                .realisation(ideaFormDto.getRealisation())
                .build();

        ideaRepository.save(idea);
    }

    public List<Idea> getAll() {
        return ideaRepository.findAll();
    }

    public Idea findById(Long idea_id) {
        return ideaRepository.getById(idea_id);
    }

    public Page<Idea> getIdeasWithPageable(Pageable pageable) {
        return this.ideaRepository.findAll(pageable);
    }
}
