package com.ihsm.esdp.users.enums;

public enum CategoryOfNotification {
    NEWS,
    PHOTOGALLERY,
    DOCUMENT
}
