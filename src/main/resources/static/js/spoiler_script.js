$(document).ready(function () {
    $('.spoiler-title').click(function () {
        $(this).toggleClass('activeToggle').next().slideToggle(300, 'linear', function () {
            $(this).toggleClass('display-block').removeAttr('style');
        });
    })
});