package com.ihsm.esdp.documents.controllers;


import com.ihsm.esdp.documents.categories.CategoriesDto;
import com.ihsm.esdp.documents.categories.CategoryService;
import com.ihsm.esdp.documents.services.DocumentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/documents")
@RequiredArgsConstructor
public class DocRestController {
    private final CategoryService categoryService;
    private final DocumentService documentService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoriesDto> displayAllCategoriesDocs(){
        return categoryService.getAll().stream()
                .map(document -> modelMapper.map(document, CategoriesDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping(path = "/download/{filename}")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable String filename) {
        try {
            String file_extension = filename.split("\\.")[filename.split("\\.").length - 1];
            String original_file_name = this.documentService.getOriginalFilename(filename);
            Path path = Paths.get("upload/doc/" + filename).toAbsolutePath();
            byte[] data = Files.readAllBytes(path);
            ByteArrayResource file = new ByteArrayResource(data);
            if (file_extension.contains("doc") || file_extension.contains("docx")){
                return this.documentService.createDocumentResponseEntity("attachment", original_file_name, MediaType.APPLICATION_OCTET_STREAM, data.length, file);
            }
            return this.documentService.createDocumentResponseEntity("inline", original_file_name, MediaType.APPLICATION_PDF, data.length, file);
        }catch (IOException ex){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
