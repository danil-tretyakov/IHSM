package com.ihsm.esdp.contactUs;

import com.ihsm.esdp.contactUs.dto.NewMessageDto;
import com.ihsm.esdp.users.service.EmailSenderService;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ContactUsMailSenderService {


    private final EmailSenderService emailSenderService;


    void sendContactUsMail(NewMessageDto newMessageDTO) {


        final SimpleMailMessage mailMessage = new SimpleMailMessage();
        String adminEmail = "ihsm.passcode.issue@gmail.com";
        mailMessage.setSubject("Обратная связь");
        mailMessage.setTo(adminEmail);
        mailMessage.setFrom(newMessageDTO.getEmail());
        mailMessage.setText(
                "Отправитель: "+newMessageDTO.getEmail()
                        +"\nТекст обращения: "+newMessageDTO.getTextOfMessage()
        );

        emailSenderService.sendEmail(mailMessage);
    }

}

