package com.ihsm.esdp.news.dtos;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.news.enums.Category;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class NewsDto extends BaseEntity {
    private String title;
    private String text;
    private LocalDateTime date;
    private String imagePath;
    private Category category;

}
