package com.ihsm.esdp.forums.message;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message,Long> {
    List<Message> findAllByTopicId(Long id);
    void deleteAllByUserId(Long id);
}
