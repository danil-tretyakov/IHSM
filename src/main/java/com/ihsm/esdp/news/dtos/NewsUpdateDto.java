package com.ihsm.esdp.news.dtos;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;


@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewsUpdateDto extends NewsDto {
    private MultipartFile image;
}
