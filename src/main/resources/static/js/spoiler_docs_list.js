$(document).ready(function () {
    $('.docs-list-title').click(function () {
        if ($('.group-docs-list').hasClass('one')){
            $('.docs-list-title').not($(this)).removeClass('active');
            $('.docs-list-body').not($(this).next()).slideUp(300);
        }
        $(this).toggleClass('active').next().slideToggle(300);
    });
});