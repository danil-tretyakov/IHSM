package com.ihsm.esdp.photoGallery.photo;

import com.ihsm.esdp.exceptions.NotFoundException;
import com.ihsm.esdp.photoGallery.gallery.Album;
import com.ihsm.esdp.photoGallery.gallery.AlbumRepository;
import com.ihsm.esdp.utility.MaltyPartFile;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PhotoService {

    private final PhotoRepository photoRepository;

    private final AlbumRepository albumRepository;

    private final MaltyPartFile maltyPartFile;

    private final String uploadPath = "upload/photo";

    public void addPhotoToAlbum(List<MultipartFile> multipartFiles,Long id) throws IOException {
        Album album = albumRepository.findById(id).orElseThrow(NotFoundException::new);
        for(MultipartFile file: multipartFiles){
             Photo photo = new Photo();
            photo.setAlbum(album);
            photo.setPhotoNamePath(maltyPartFile.uploadFile(uploadPath, file));
            photoRepository.save(photo);
        }
    }

    public Photo getPhotoById(Long id) {
        return photoRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public void deletePhoto(Long id) {
        Photo photo =photoRepository.findById(id).orElseThrow(NotFoundException::new);
        photoRepository.delete(photo);
    }
}
