package com.ihsm.esdp.users.controllers;

import com.ihsm.esdp.exceptions.EmployeeNotFoundException;
import com.ihsm.esdp.exceptions.NotFoundException;
import com.ihsm.esdp.users.dtos.employeeActionsDtos.EmployeeActionsDto;
import com.ihsm.esdp.users.dtos.employeeActionsDtos.postDtos.*;
import com.ihsm.esdp.users.service.EmployeeActionsService;
import com.ihsm.esdp.users.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/employees/actions")
@RequiredArgsConstructor
public class EmployeeActionsController {

    private final EmployeeService employeeService;

    private final EmployeeActionsService employeeActionsService;

    @PostMapping("/transaction/create")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String addNewTransaction(RedirectAttributes attributes, @ModelAttribute("transaction") TransactionPostDto transactionPostDto){
        try {
            this.employeeService.setNewTransactions(transactionPostDto);

        }catch (NotFoundException ex){
            attributes.addFlashAttribute("dep_not_found", "Подразделение не найдено");
            return "redirect:/employees/management/" + transactionPostDto.getEmployeeId();
        }catch (EmployeeNotFoundException e){
            attributes.addFlashAttribute("employee_not_found", "Произошла ошибка. Сотрудник не найден");
            return  "redirect:/employees/management/" + transactionPostDto.getEmployeeId();
        }
        return "redirect:/employees/management/" + transactionPostDto.getEmployeeId();
    }

    @PostMapping("/dismiss")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String dismissEmployee(RedirectAttributes attributes, @RequestParam Long id){
        try{
            this.employeeService.setDismissalDate(id);

        }catch (EmployeeNotFoundException ex){
            attributes.addFlashAttribute("employee_not_found", "Произошла ошибка. Сотрудник не найден");
            return  "redirect:/employees/management/" + id;
        }
        return "redirect:/employees/management/" + id;
    }

    @PostMapping("/vacation/give")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String setVacationToEmployee(RedirectAttributes attributes, @ModelAttribute("vacation") VacationOrderPostDto vacationOrderPostDto){
        try {
            this.employeeService.setVacationOrder(vacationOrderPostDto);

        }catch (EmployeeNotFoundException e){
            attributes.addFlashAttribute("employee_not_found", "Произошла ошибка. Сотрудник не найден");
        }
        return "redirect:/employees/management/" + vacationOrderPostDto.getEmployeeId();
    }

    @PostMapping("/vacation/add")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String setVacation(RedirectAttributes attributes, @ModelAttribute("vacation") VacationPostDto vacationPostDto){
        try{
            this.employeeService.addNewVacationDates(vacationPostDto);
        }catch (EmployeeNotFoundException ex){
            attributes.addFlashAttribute("employee_not_found", "Произошла ошибка. Сотрудник не найден");
        }
        return "redirect:/employees/management/" + vacationPostDto.getEmployeeId();
    }

    @PostMapping("/truancy/add")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String setTruancy(RedirectAttributes attributes, @ModelAttribute("truancy") TruancyPostDto truancyPostDto){
        try{
            this.employeeService.addNewTruancy(truancyPostDto);
        }catch (EmployeeNotFoundException ex){
            attributes.addFlashAttribute("employee_not_found", "Произошла ошибка. Сотрудник не найден");
        }
        return "redirect:/employees/management/" + truancyPostDto.getEmployeeId();
    }

    @PostMapping("/business_trip/add")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String setBusinessTrip(RedirectAttributes attributes, @ModelAttribute("business_trip") BusinessTripPostDto businessTripPostDto){
        try{
            this.employeeService.addNewBusinessTrip(businessTripPostDto);
        }catch (EmployeeNotFoundException ex){
            attributes.addFlashAttribute("employee_not_found", "Произошла ошибка. Сотрудник не найден");
        }
        return "redirect:/employees/management/" + businessTripPostDto.getEmployeeId();
    }

    @PostMapping("/change")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String updateDataOfActions(RedirectAttributes attributes, @ModelAttribute("actionsDtos") EmployeeActionsDto employeeActionsDto){
        try{
            this.employeeActionsService.updateEmployeeActionsData(employeeActionsDto);
        }catch (EmployeeNotFoundException ex){
            attributes.addFlashAttribute("employee_not_found", "Произошла ошибка. Сотрудник не найден");
        }
        return "redirect:/employees/management/" + employeeActionsDto.getEmployeeId() + "/change";
    }

    @PostMapping("/remove-data")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String deleteDataOfTheEmployee(RedirectAttributes attributes, @RequestParam Long employeeId){
        try{
            this.employeeActionsService.deleteEmployeeInfoFromDB(employeeId);
        }catch (EmployeeNotFoundException ex){
            attributes.addFlashAttribute("employee_not_found", "Произошла ошибка. Сотрудник не найден");
        }
        return "redirect:/employees/management";
    }
}
