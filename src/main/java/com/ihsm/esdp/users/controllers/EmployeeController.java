package com.ihsm.esdp.users.controllers;

import com.ihsm.esdp.users.dtos.*;
import com.ihsm.esdp.users.dtos.employeeActionsDtos.postDtos.*;
import com.ihsm.esdp.users.information.*;
import com.ihsm.esdp.users.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@Controller
@RequestMapping("/employees/management")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String showDefaultPage(Model model){
        model.addAttribute("default_message", "Выберите сотрудника из списка слева");
        model.addAttribute("employees_list", this.employeeService.getAllEmployeesNames());
        return "personnelManagement/employee-info";
    }

    @GetMapping("/{emplId}")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String showPageWithInfo(Model model, @PathVariable Long emplId){
        Optional<Employee> employee = this.employeeService.getEmployeeById(emplId);
        model.addAttribute("employees_list", this.employeeService.getAllEmployeesNames());
        if (employee.isEmpty()){
            model.addAttribute("default_message", "Извините, но сотрудник не найден. Выберите сотрудника из списка слева");
            return "personnelManagement/employee-info";
        }
        model.addAttribute("departments", this.employeeService.getAllDepartments());
        this.employeeService.sortEmployeeCollections(employee.get());
        model.addAttribute("employee", modelMapper.map(employee.get(), EmployeeDto.class));
        model.addAttribute("transaction", new TransactionPostDto());
        model.addAttribute("vacation_order", new VacationOrderPostDto());
        model.addAttribute("vacation", new VacationPostDto());
        model.addAttribute("truancy", new TruancyPostDto());
        model.addAttribute("business_trip", new BusinessTripPostDto());
        return "personnelManagement/employee-info";
    }

    @GetMapping("/{id}/change")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String showUpdateInfoPage(Model model, @PathVariable Long id){
        model.addAttribute("employees_list", this.employeeService.getAllEmployeesNames());
        Optional<Employee> employeeById = this.employeeService.getEmployeeById(id);
        model.addAttribute("departments", this.employeeService.getAllDepartments());
        if (employeeById.isEmpty()){
            model.addAttribute("default_message", "Извините, но страница не найдена");
            return "personnelManagement/employee-info";
        }
        this.employeeService.sortEmployeeCollections(employeeById.get());
        EmployeeDto employeeDto = modelMapper.map(employeeById.get(), EmployeeDto.class);
        model.addAttribute("employee", employeeDto);
        model.addAttribute("actionsDtos", this.employeeService.getEmployeeActionsDto(employeeDto));
        return "personnelManagement/update-empl-info";
    }

    @PostMapping("/change")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String updateEmployeeData(@ModelAttribute("employee") EmployeeDto employeeDto){
        Long id = this.employeeService.updateInfo(employeeDto);
        return "redirect:/employees/management/" + id;
    }

}
