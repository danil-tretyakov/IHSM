package com.ihsm.esdp.users.dtos.employeeActionsDtos.postDtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TruancyPostDto {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    private String reason;

    private Long employeeId;
}
