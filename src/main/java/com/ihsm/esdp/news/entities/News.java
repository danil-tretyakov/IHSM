package com.ihsm.esdp.news.entities;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.news.enums.Category;
import com.ihsm.esdp.users.User;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "news")
public class News extends BaseEntity {

    @Column(name = "title")
    private String title;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String text;

    @ManyToOne
    private User user;

    @Column
    private LocalDateTime date;

    @Column(name = "image")
    private String imagePath;


    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private Category category;

}
