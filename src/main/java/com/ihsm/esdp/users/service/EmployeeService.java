package com.ihsm.esdp.users.service;

import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.departments.DepartmentService;
import com.ihsm.esdp.exceptions.EmployeeNotFoundException;
import com.ihsm.esdp.exceptions.NotFoundException;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.dtos.*;
import com.ihsm.esdp.users.dtos.employeeActionsDtos.*;
import com.ihsm.esdp.users.dtos.employeeActionsDtos.deleteDtos.*;
import com.ihsm.esdp.users.dtos.employeeActionsDtos.postDtos.*;
import com.ihsm.esdp.users.information.*;
import com.ihsm.esdp.users.repositories.*;
import com.ihsm.esdp.users.repositories.employeeActionsRepositories.*;
import com.ihsm.esdp.utility.MaltyPartFile;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final EmployeePositionRepository employeePositionRepository;

    private final SubjectRepository subjectRepository;

    private final EducationRepository educationRepository;

    private final AwardRepository awardRepository;

    private final FamilyMemberRepository familyMemberRepository;

    private final ScienceDegreeRepository scienceDegreeRepository;

    private final EmploymentRepository employmentRepository;

    private final DepartmentService departmentService;

    private final UserService userService;

    private final TransactionRepository transactionRepository;

    private final VacationRepository vacationRepository;

    private final VacationOrderRepository vacationOrderRepository;

    private final TruancyRepository truancyRepository;

    private final BusinessTripRepository businessTripRepository;

    private final ModelMapper modelMapper = new ModelMapper();

    private final MaltyPartFile maltyPartFile;

    public List<EmployeeNameDto> getAllEmployeesNames() {
        List<String> employeesFullNamesWithCommasList = this.employeeRepository.findAllByNames();
        List<EmployeeNameDto> employeesNamesList = new ArrayList<>();
        for (String row : employeesFullNamesWithCommasList) {
            String[] splittedRow = row.split(",");
            EmployeeNameDto employeeNameDto = EmployeeNameDto.builder()
                    .id(Long.parseLong(splittedRow[0]))
                    .surname(splittedRow[1])
                    .name(splittedRow[2])
                    .build();
            if (splittedRow.length == 3 || splittedRow[3].equalsIgnoreCase("null")){
                employeesNamesList.add(employeeNameDto);
                continue;
            }
            employeeNameDto.setPatronymic(splittedRow[3]);
            employeesNamesList.add(employeeNameDto);
        }
        employeesNamesList.removeIf(employeeNameDto -> "Супер".equalsIgnoreCase(employeeNameDto.getSurname()));
        employeesNamesList.sort(Comparator.comparing(EmployeeNameDto::getSurname));
        return employeesNamesList;
    }

    public Optional<Employee> getEmployeeById(Long id) {
        return this.employeeRepository.findById(id);
    }

    public List<Department> getAllDepartments() {
        return this.departmentService.getAll();
    }

    public Long updateInfo(EmployeeDto employeeDto) {
        User userFromDB = this.userService.findUserForEmployeeDataById(employeeDto.getId()).orElseThrow(EmployeeNotFoundException::new);

        userFromDB.setSurname(employeeDto.getUser().getSurname());
        userFromDB.setName(employeeDto.getUser().getName());
        employeeDto.setUser(userFromDB);
        if (employeeDto.getEmployeePosition().getName() != null) employeeDto.setEmployeePosition(checkEmployeePositionIfExist(employeeDto.getEmployeePosition()));
        if (employeeDto.getSubject().getName() != null) employeeDto.setSubject(checkSubjectIfExist(employeeDto.getSubject()));
        if (employeeDto.getDepartment().getName() != null) employeeDto.setDepartment(this.departmentService.getDepartmentByName(employeeDto.getDepartment().getName()).orElse(new Department()));
        dropUnnecessaryListsElements(employeeDto);
        deleteEmployments(employeeDto);
        Employee employeeToSave = modelMapper.map(employeeDto, Employee.class);
        employeeToSave.setImagePath(setNewUserPhoto(employeeDto));
        setEmployeesToHisFields(employeeToSave);
        return this.employeeRepository.save(employeeToSave).getId();
    }

    private void deleteEmployments(EmployeeDto employeeDto) {
        if (employeeDto.getEmployments() != null && !employeeDto.getEmployments().isEmpty()){
            employeeDto.getEmployments().removeIf(employmentDto -> !employmentDto.isToDelete());
            for (EmploymentDto employmentDto : employeeDto.getEmployments()) {
                if (this.employmentRepository.findById(employmentDto.getId()).isPresent()) {
                    Employment employment = this.employmentRepository.findById(employmentDto.getId()).get();
                    deleteAssociativeTransaction(employment);
                    updateEmploymentDateOut(employment);
                    this.employmentRepository.deleteById(employmentDto.getId());
                }

            }
        }
    }

    private void deleteAssociativeTransaction(Employment employment) {
        Optional<Transaction> optionalTransaction = this.transactionRepository.findByDateOfTransactionAndEmployee_Id(employment.getTransferDateIn(), employment.getEmployee().getId());
        optionalTransaction.ifPresent(this.transactionRepository::delete);
    }


    private void updateEmploymentDateOut(Employment employment) {
        Optional<Employment> previousEmployment = this.employmentRepository.findByTransferDateOutAndEmployee_Id(employment.getTransferDateIn(), employment.getEmployee().getId());
        previousEmployment.ifPresent(value -> this.employmentRepository.setNewDateOutOfEmployment(value.getId()));
    }

    private void dropUnnecessaryListsElements(EmployeeDto employeeDto) {
        employeeDto.setEducations(dropUnnecessaryEducation(employeeDto.getEducations()));
        employeeDto.setFamily(dropUnnecessaryFamilyMembers(employeeDto.getFamily()));
        employeeDto.setScienceDegree(dropUnnecessaryDegrees(employeeDto.getScienceDegree()));
        employeeDto.setAwards(dropUnnecessaryAwards(employeeDto.getAwards()));
    }

    private void setEmployeesToHisFields(Employee employeeToSave) {
        if (employeeToSave.getEducations() != null && !employeeToSave.getEducations().isEmpty()){
            employeeToSave.getEducations().forEach(education -> education.setEmployee(employeeToSave));
        }
        if (employeeToSave.getFamily() != null && !employeeToSave.getFamily().isEmpty()) {
            employeeToSave.getFamily().forEach(familyMember -> familyMember.setEmployee(employeeToSave));
        }
        if (employeeToSave.getScienceDegree() != null && !employeeToSave.getScienceDegree().isEmpty()){
            employeeToSave.getScienceDegree().forEach(scienceDegree -> scienceDegree.setEmployee(employeeToSave));
        }
        if (employeeToSave.getAwards() != null && !employeeToSave.getAwards().isEmpty()){
            employeeToSave.getAwards().forEach(award -> award.setEmployee(employeeToSave));
        }
    }

    private Subject checkSubjectIfExist(Subject subject) {
        return this.subjectRepository.findByName(subject.getName()).orElse(subject);
    }

    private EmployeePosition checkEmployeePositionIfExist(EmployeePosition employeePosition) {
        return this.employeePositionRepository.findByName(employeePosition.getName()).orElse(employeePosition);
    }

    private List<EducationDto> dropUnnecessaryEducation(List<EducationDto> educationDtos){
        if (educationDtos != null && !educationDtos.isEmpty()){
            educationDtos.removeIf(educationDto -> educationDto.isNew() && educationDto.isToDelete());
            for (EducationDto education : educationDtos) {
                if (education.isToDelete()){
                    if(this.educationRepository.findById(education.getId()).isPresent()){
                        this.educationRepository.deleteById(education.getId());
                    }
                }
            }
            educationDtos.removeIf(EducationDto::isToDelete);
        }
        return educationDtos;
    }

    private List<AwardDto> dropUnnecessaryAwards(List<AwardDto> awards) {
        if (awards != null && !awards.isEmpty()){
            awards.removeIf(awardDto -> awardDto.isNew() && awardDto.isToDelete());
            for (AwardDto awardDto: awards) {
                if (awardDto.isToDelete()){
                    if (this.awardRepository.findById(awardDto.getId()).isPresent()){
                        this.awardRepository.deleteById(awardDto.getId());
                    }
                }
            }
            awards.removeIf(AwardDto::isToDelete);
        }

        return awards;
    }

    private List<ScienceDegreeDto> dropUnnecessaryDegrees(List<ScienceDegreeDto> scienceDegree) {
        if (scienceDegree != null && !scienceDegree.isEmpty()){
            scienceDegree.removeIf(scienceDegreeDto -> scienceDegreeDto.isNew() && scienceDegreeDto.isToDelete());
            for (ScienceDegreeDto scienceDegreeDto: scienceDegree) {
                if (scienceDegreeDto.isToDelete()){
                    if (this.scienceDegreeRepository.findById(scienceDegreeDto.getId()).isPresent()){
                        this.scienceDegreeRepository.deleteById(scienceDegreeDto.getId());
                    }
                }
            }
            scienceDegree.removeIf(ScienceDegreeDto::isToDelete);
        }
        return scienceDegree;
    }

    private List<FamilyMemberDto> dropUnnecessaryFamilyMembers(List<FamilyMemberDto> family) {
        if (family != null && !family.isEmpty()){
            family.removeIf(familyMemberDto -> familyMemberDto.isNew() && familyMemberDto.isToDelete());
            for (FamilyMemberDto familyMemberDto : family) {
                if (familyMemberDto.isToDelete()){
                    if (this.familyMemberRepository.findById(familyMemberDto.getId()).isPresent()){
                        this.familyMemberRepository.deleteById(familyMemberDto.getId());
                    }
                }
            }
            family.removeIf(FamilyMemberDto::isToDelete);
        }

        return family;
    }

    public void setNewTransactions(TransactionPostDto transactionPostDto) throws EmployeeNotFoundException, NotFoundException{
        Employee employee = this.employeeRepository.findById(transactionPostDto.getEmployeeId()).orElseThrow(EmployeeNotFoundException::new);
        List<Employment> employments = employee.getEmployments();
        if (!employments.isEmpty()){
            employments.sort(Comparator.comparing(Employment::getId));
            employments.get(employments.size() - 1).setTransferDateOut(transactionPostDto.getDateOfTransaction());
        }
        Department department = this.departmentService.getById(transactionPostDto.getDepartment().getId()).orElseThrow(NotFoundException::new);
        employments.add(Employment.builder()
                .department(department)
                .employee(employee)
                .employeePosition(checkEmployeePositionIfExist(transactionPostDto.getEmployeePosition()))
                .transferDateIn(transactionPostDto.getDateOfTransaction())
                .build());
        this.transactionRepository.save(Transaction.builder()
                .dateOfTransaction(transactionPostDto.getDateOfTransaction())
                .department(department)
                .employeePosition(checkEmployeePositionIfExist(transactionPostDto.getEmployeePosition()))
                .employee(employee)
                .orderNumber(transactionPostDto.getOrderNumber())
                .build());
        this.employmentRepository.saveAll(employments);
        this.employeeRepository.setNewEmployeePosition(employee.getId(), checkEmployeePositionIfExist(transactionPostDto.getEmployeePosition()));
    }

    public void setDismissalDate(Long employeeId) throws EmployeeNotFoundException {
        Employee employee = this.employeeRepository.findById(employeeId).orElseThrow(EmployeeNotFoundException::new);
        employee.setDismissalDate(LocalDate.now());
        this.employeeRepository.save(employee);
    }

    public void setVacationOrder(VacationOrderPostDto vacationOrderPostDto) throws EmployeeNotFoundException {
        Employee employee = this.employeeRepository.findById(vacationOrderPostDto.getEmployeeId()).orElseThrow(EmployeeNotFoundException::new);
        this.vacationOrderRepository.save(VacationOrder.builder()
                .employee(employee)
                .orderNumber(vacationOrderPostDto.getOrderNumber())
                .startVacation(vacationOrderPostDto.getStartVacation())
                .endVacation(vacationOrderPostDto.getEndVacation())
                .vacationType(vacationOrderPostDto.getVacationType())
                .build());
    }

    public void addNewVacationDates(VacationPostDto vacationPostDto) throws EmployeeNotFoundException {
        Employee employee = this.employeeRepository.findById(vacationPostDto.getEmployeeId()).orElseThrow(EmployeeNotFoundException::new);
        this.vacationRepository.save(Vacation.builder()
                .startVacation(vacationPostDto.getStartVacation())
                .endVacation(vacationPostDto.getEndVacation())
                .employee(employee)
                .build());
    }

    public void addNewTruancy(TruancyPostDto truancyPostDto) throws EmployeeNotFoundException {
        Employee employee = this.employeeRepository.findById(truancyPostDto.getEmployeeId()).orElseThrow(EmployeeNotFoundException::new);
        this.truancyRepository.save(Truancy.builder()
                .date(truancyPostDto.getDate())
                .reason(truancyPostDto.getReason())
                .employee(employee)
                .build());
    }

    public void addNewBusinessTrip(BusinessTripPostDto businessTripPostDto) throws EmployeeNotFoundException {
        Employee employee = this.employeeRepository.findById(businessTripPostDto.getEmployeeId()).orElseThrow(EmployeeNotFoundException::new);
        this.businessTripRepository.save(BusinessTrip.builder()
                .dateOfBeginning(businessTripPostDto.getDateOfBeginning())
                .dateOfEnding(businessTripPostDto.getDateOfEnding())
                .orderNumber(businessTripPostDto.getOrderNumber())
                .purposeOfTheTrip(businessTripPostDto.getPurposeOfTheTrip())
                .employee(employee)
                .build());
    }

    public void sortEmployeeCollections(Employee employee) {
        employee.getEmployments().sort(Comparator.comparing(Employment::getId));
        employee.getTransactions().sort(Comparator.comparing(Transaction::getId));
        employee.getVacationOrders().sort(Comparator.comparing(VacationOrder::getId));
        employee.getBusinessTrips().sort((Comparator.comparing(BusinessTrip::getId)));
        employee.getTruancyList().sort((Comparator.comparing(Truancy::getId)));
        employee.getVacation().sort(Comparator.comparing(Vacation::getId));
        employee.getAwards().sort(Comparator.comparing(Award::getDate));
        employee.getFamily().sort(Comparator.comparing(FamilyMember::getId));
        employee.getEducations().sort(Comparator.comparing(Education::getId));
    }

    public EmployeeActionsDto getEmployeeActionsDto(EmployeeDto employeeDto) {
        return EmployeeActionsDto.builder()
                .employeeId(employeeDto.getId())
                .businessTrips(employeeDto.getBusinessTrips().stream()
                        .map(businessTrip -> modelMapper.map(businessTrip, BusinessTripDeleteDto.class))
                        .collect(Collectors.toList()))
                .transactions(employeeDto.getTransactions().stream()
                        .map(transaction -> modelMapper.map(transaction, TransactionDeleteDto.class))
                        .collect(Collectors.toList()))
                .truancyList(employeeDto.getTruancyList().stream()
                        .map(truancy -> modelMapper.map(truancy, TruancyDeleteDto.class))
                        .collect(Collectors.toList()))
                .vacationOrders(employeeDto.getVacationOrders().stream()
                        .map(vacationOrder -> modelMapper.map(vacationOrder, VacationOrderDeleteDto.class))
                        .collect(Collectors.toList()))
                .vacations(employeeDto.getVacation().stream()
                        .map(vacation -> modelMapper.map(vacation, VacationDeleteDto.class))
                        .collect(Collectors.toList()))
                .build();
    }


    private String setNewUserPhoto(EmployeeDto employeeDto) {
        if (!employeeDto.getNewPhoto().isEmpty()){
            try{
                return this.maltyPartFile.uploadFile("upload/users", employeeDto.getNewPhoto());

            }catch (IOException ex){
                return employeeDto.getImagePath();
            }
        }

        return employeeDto.getImagePath();
    }

    public void deleteEmployeeInfoFromDB(Long employeeId) {
        this.employeeRepository.deleteById(employeeId);
    }
}
