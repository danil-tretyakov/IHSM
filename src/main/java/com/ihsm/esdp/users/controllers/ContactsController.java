package com.ihsm.esdp.users.controllers;

import com.ihsm.esdp.departments.DepartmentDto;
import com.ihsm.esdp.exceptions.EmployeeNotFoundException;
import com.ihsm.esdp.users.dtos.contacts.ContactDto;
import com.ihsm.esdp.users.dtos.contacts.ContactUpdateDto;
import com.ihsm.esdp.users.information.Employee;
import com.ihsm.esdp.users.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.stream.Collectors;

@Controller
@RequestMapping("/contacts")
@RequiredArgsConstructor
public class ContactsController {

    private final UserService userService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    public String showListOfEmployees(Model model){
        model.addAttribute("departments",
                this.userService.getAllDepartments().stream()
                    .map(department -> modelMapper.map(department, DepartmentDto.class))
                    .collect(Collectors.toList()));
        model.addAttribute("employees",
                this.userService.getAllEmployees().stream()
                    .map(employee -> modelMapper.map(employee, ContactDto.class))
                    .collect(Collectors.toList()));
        return "contacts/contacts-list";
    }

    @GetMapping("/{employeeId}")
    public String showEmployeeInfo(Model model, @PathVariable Long employeeId){
        Employee employee = this.userService.findEmployeeId(employeeId).orElseThrow(EmployeeNotFoundException::new);
        model.addAttribute("employee", modelMapper.map(employee, ContactDto.class));
        return "contacts/contact-info";
    }

    @GetMapping("/search")
    public String findContacts(Model model, @RequestParam String param){
        model.addAttribute("employees",
                this.userService.findEmployeesByParameter(param).stream()
                    .map(employee -> modelMapper.map(employee, ContactDto.class))
                    .collect(Collectors.toList()));
        return "contacts/result-of-contacts-search";
    }

    @GetMapping("/{employeeId}/update")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String showUpdateInfoPage(Model model, @PathVariable Long employeeId){
        model.addAttribute("contact",
                modelMapper.map(this.userService.findEmployeeId(employeeId).orElseThrow(EmployeeNotFoundException::new), ContactUpdateDto.class));
        model.addAttribute("departments",
                this.userService.getAllDepartments().stream()
                    .map(department -> modelMapper.map(department, DepartmentDto.class))
                    .collect(Collectors.toList()));
        return "contacts/update-contact-info";
    }

    @PostMapping("/{employeeId}/update")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String updateInfoAboutContact(RedirectAttributes redirAttrs,
                                         @PathVariable Long employeeId,
                                         @ModelAttribute(name = "contact") ContactUpdateDto contactDto){
        redirAttrs.addFlashAttribute("contact", modelMapper.map(contactDto, ContactDto.class));
        try{
            this.userService.updateNewEmployeeInfo(contactDto, employeeId);
        }catch (EmployeeNotFoundException ex){
            return "redirect:/contacts/";
        }
        return "redirect:/contacts/" + contactDto.getId();
    }
}
