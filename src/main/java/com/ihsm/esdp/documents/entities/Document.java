package com.ihsm.esdp.documents.entities;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.documents.categories.CategoryDocs;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.time.LocalDateTime;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "documents")
public class Document extends BaseEntity {

    @Column(name = "document_name")
    private String documentName;

    @ManyToOne
    private Department department;

    @Value("upload.path")
    private String pathUploadFile;

    @ManyToOne
    private CategoryDocs categoryDocs;

    @Column(name = "file_date")
    private LocalDateTime fileDate;




}
