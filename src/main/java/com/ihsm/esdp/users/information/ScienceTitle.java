package com.ihsm.esdp.users.information;

public enum ScienceTitle {
    NOT_SPECIFIED("Не указано"), DOCENT("Доцент"), PROFESSOR("Профессор"), ACTING_DOCENT("И.о. доцента"), ACTING_PROFESSOR("И.о. профессора");
    private final String name;

    ScienceTitle(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
