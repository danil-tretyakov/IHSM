package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.departments.Department;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "transactions")
public class Transaction extends BaseEntity {

    @Column
    private long orderNumber;

    @Column
    private LocalDate dateOfTransaction;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private EmployeePosition employeePosition;

    @ManyToOne
    private Department department;

    @ManyToOne
    private Employee employee;

}
