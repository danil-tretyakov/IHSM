package com.ihsm.esdp.users.repositories.employeeActionsRepositories;

import com.ihsm.esdp.users.information.Vacation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VacationRepository extends JpaRepository<Vacation, Long> {
}
