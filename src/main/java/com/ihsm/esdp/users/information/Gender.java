package com.ihsm.esdp.users.information;

public enum Gender {
    NOT_SPECIFIED("Не указано"), MALE("Мужчина"),FEMALE("Женщина");
    private final String name;

    Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
