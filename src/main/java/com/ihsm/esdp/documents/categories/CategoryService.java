package com.ihsm.esdp.documents.categories;

import com.ihsm.esdp.departments.Department;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepo categoryRepo;

    public List<CategoryDocs> getCategory(){
        return this.categoryRepo.findAll();
    }

    public List<CategoryDocs> getAll(){
        return (List<CategoryDocs>) categoryRepo.findAll();
    }

    public Optional<CategoryDocs> getCategoryById(Long id){
        return this.categoryRepo.findById(id);
    }
}
