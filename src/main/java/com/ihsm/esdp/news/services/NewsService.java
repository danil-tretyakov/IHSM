package com.ihsm.esdp.news.services;

import com.ihsm.esdp.news.dtos.NewsPostDto;
import com.ihsm.esdp.news.dtos.NewsUpdateDto;

import com.ihsm.esdp.news.enums.Category;

import com.ihsm.esdp.news.entities.News;
import com.ihsm.esdp.news.repositories.NewsRepository;
import com.ihsm.esdp.users.enums.CategoryOfNotification;
import com.ihsm.esdp.users.security.MySecurityUser;
import com.ihsm.esdp.users.service.EmailSenderService;
import com.ihsm.esdp.utility.MaltyPartFile;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NewsService {

    private final EmailSenderService emailSenderService;

    private final NewsRepository newsRepository;

    private final MaltyPartFile maltyPartFile;

    private final String uploadPath = "upload/news";

    public List<News> getLastFiveNews() {
        Pageable pageable = PageRequest.of(0,5, Sort.by(Sort.Order.desc("date")));
        return this.newsRepository.findAll(pageable).getContent();
    }

    public Page<News> getAllNewsByPages(Pageable pageable) {
        Pageable pageableWithSort = PageRequest.of(pageable.getPageNumber(),
                pageable.getPageSize(), Sort.by(Sort.Order.desc("date")));
        return this.newsRepository.findAll(pageableWithSort);
    }

    public void createNewPost(NewsPostDto newsDto, MySecurityUser user) throws IOException {
        LocalDateTime ldt = LocalDateTime.now();
        Path path  = Path.of(uploadPath).toAbsolutePath();
        News newPost = News.builder()
                .date(ldt)
                .title(newsDto.getTitle())
                .text(newsDto.getText())
                .user(user.getUser())
                .imagePath(this.maltyPartFile.uploadFile(path.toString(), newsDto.getImage()))
                .category(newsDto.getCategory())
                .build();
        Long savedPostId = this.newsRepository.save(newPost).getId();
        this.emailSenderService.sendNotificationToAllUsers(newsDto.getTitle(), CategoryOfNotification.NEWS, savedPostId, "");
    }


    public Optional<News> getPostById(Long id) {
        return this.newsRepository.findById(id);
    }

    public void updatePost(Long newsId, NewsUpdateDto newsDto) throws IOException{

        if (newsDto.getImage().isEmpty()){
            this.newsRepository.updatePostData(newsId, newsDto.getTitle(), newsDto.getText(), newsDto.getImagePath());
            return;
        }
        if (newsDto.getCategory() == Category.PhotoGallery){
            this.newsRepository.updatePostData(newsId, newsDto.getTitle(), newsDto.getText(), maltyPartFile.uploadFile("upload/photo", newsDto.getImage()));
            return;
        }
        this.newsRepository.updatePostData(newsId, newsDto.getTitle(), newsDto.getText(), maltyPartFile.uploadFile(uploadPath, newsDto.getImage()));
    }


    public Page<News> getOurLifeNews(Pageable pageable){

        Pageable pageableWithSort = PageRequest.of(pageable.getPageNumber(),
                pageable.getPageSize(), Sort.by(Sort.Order.desc("date")));
        
        return this.newsRepository.findByCategory(pageableWithSort,  Category.OurLife);
    }

    public void deletePostById(Long postId) {
        this.newsRepository.deleteById(postId);
    }
}
