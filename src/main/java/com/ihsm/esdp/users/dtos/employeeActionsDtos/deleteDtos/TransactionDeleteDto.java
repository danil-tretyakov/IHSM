package com.ihsm.esdp.users.dtos.employeeActionsDtos.deleteDtos;

import com.ihsm.esdp.users.information.Transaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDeleteDto extends Transaction {
    private boolean toDelete;
}
