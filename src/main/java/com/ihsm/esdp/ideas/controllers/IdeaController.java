package com.ihsm.esdp.ideas.controllers;

import com.ihsm.esdp.ideas.dtos.IdeaFormDto;
import com.ihsm.esdp.ideas.entities.Idea;
import com.ihsm.esdp.ideas.services.IdeaService;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.security.MySecurityUser;
import com.ihsm.esdp.users.service.UserService;
import com.ihsm.esdp.utility.PaginationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@RequiredArgsConstructor
@Controller
@RequestMapping("/ideas")
public class IdeaController {

    private final IdeaService ideaService;
    private final UserService userService;
    private final PaginationService paginationService;

    @GetMapping("/idea")
    public String getIdeaForm(Model model, @PageableDefault(size = 5) Pageable pageable, Authentication authentication){
        model.addAttribute("idea", new IdeaFormDto());
        model.addAttribute("departments", ideaService.getDepartments());
        Page<Idea> ideasList = this.ideaService.getIdeasWithPageable(pageable);
        model.addAttribute("ideas", ideasList);
        MySecurityUser mySecurityUser = (MySecurityUser) authentication.getPrincipal();
        model.addAttribute("user_id", userService.findUserByMail(mySecurityUser.getUser().getEmail()).get().getId());
        this.paginationService.setPaginationVariablesInModel(model, ideasList.getPageable(), ideasList.getTotalPages());
        return "idea/bank-idea";
    }

    @PostMapping("/newIdea")
    public String createIdea(@ModelAttribute ("idea") IdeaFormDto ideaFormDto){
        ideaService.addIdea(ideaFormDto);
        return "redirect:/ideas/idea";
    }

}
