package com.ihsm.esdp.birthday;

import com.ihsm.esdp.users.information.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface BirthdayRepository extends JpaRepository<Employee, Long> {

    @Query("select e from employee e where month(e.birthDate) = ?1 and day(e.birthDate) = ?2")
    List<Employee> findByBirthDate(int monthValue, int dayOfMonth);
}
