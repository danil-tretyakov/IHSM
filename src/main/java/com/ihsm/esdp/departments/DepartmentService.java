package com.ihsm.esdp.departments;

import com.ihsm.esdp.documents.repositories.DocumentRepository;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Data
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    private final DocumentRepository documentRepository;

    public List<Department> getAll(){
        return departmentRepository.findAll();
    }

    public Optional<Department> getById(Long id){
        return departmentRepository.findById(id);
    }

    public Optional<Department> getDepartmentByName(String name) {
        return this.departmentRepository.findByName(name);
    }

}


