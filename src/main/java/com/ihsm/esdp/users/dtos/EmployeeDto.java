package com.ihsm.esdp.users.dtos;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.information.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto extends BaseEntity {

    private User user;

    private String patronymic;

    private Subject subject;

    private Department department;

    private EmployeePosition employeePosition;

    private TypeOfEmployment typeOfEmployment;

    private List<ScienceDegreeDto> scienceDegree;

    private ScienceTitle scienceTitle;

    private PassportData passportData;

    private String militaryRank;

    private List<EducationDto> educations;

    private int workExperience;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startWork;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate DismissalDate;

    private List<EmploymentDto> employments;

    private List<AwardDto> awards;

    private Gender gender;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    private List<FamilyMemberDto> family;

    private MaritalStatus maritalStatus;
    
    private String Address;

    private String roomNumber;

    private List<Vacation> vacation;

    private String phoneNumber;

    private String imagePath;

    private List<Transaction> transactions;

    private List<VacationOrder> vacationOrders;

    private List<Truancy> truancyList;

    private List<BusinessTrip> businessTrips;

    private MultipartFile newPhoto;
}
