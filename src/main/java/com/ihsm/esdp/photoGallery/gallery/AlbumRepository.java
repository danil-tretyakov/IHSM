package com.ihsm.esdp.photoGallery.gallery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AlbumRepository extends JpaRepository<Album,Long> {
    Optional<Album>findByAlbumName(String galleryName);
    Page<Album>findAll(Pageable pageable);
}
