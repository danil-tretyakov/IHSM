package com.ihsm.esdp.documents.repositories;

import com.ihsm.esdp.documents.entities.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface DocumentRepository extends JpaRepository<Document, Long> {

    Page<Document> findAllByCategoryDocsId(Long categoryDocs_id, Pageable pageable);

    List<Document> findAllByDepartment_Id(Long id);

    List<Document> findAllByDocumentNameIsContaining(String name);
}

