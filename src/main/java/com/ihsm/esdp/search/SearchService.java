package com.ihsm.esdp.search;

import com.ihsm.esdp.documents.entities.Document;
import com.ihsm.esdp.documents.repositories.DocumentRepository;
import com.ihsm.esdp.news.entities.News;
import com.ihsm.esdp.news.repositories.NewsRepository;
import com.ihsm.esdp.users.information.Employee;
import com.ihsm.esdp.users.repositories.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class SearchService {
    private final DocumentRepository documentRepository;
    private final NewsRepository newsRepository;
    private final EmployeeRepository employeeRepository;

    public List<Document> getSearchInDocuments(String name){
        return this.documentRepository.findAllByDocumentNameIsContaining(name);
    }

    public List<Employee> findEmployeesByParameter(String param) {
        return this.employeeRepository.findAllByUser_NameContainingOrUser_SurnameContainingOrPatronymicContaining(param, param, param);
    }

    public List<News> getSearchNews(String title){
        return this.newsRepository.findAllByTitleIsContaining(title);
    }
}
