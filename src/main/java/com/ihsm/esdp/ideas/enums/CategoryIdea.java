package com.ihsm.esdp.ideas.enums;

public enum CategoryIdea {
    MOTIVATION("Мотивация"), QUALITY_OF_EDUCATION("Качество образования"), OTHER("Другое");

    private final String name;

    CategoryIdea(String name){
        this.name =name;
    }

    public String getName(){
        return name;
    }

}
