package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employee_positions")
public class EmployeePosition extends BaseEntity {
    private String name;
}
