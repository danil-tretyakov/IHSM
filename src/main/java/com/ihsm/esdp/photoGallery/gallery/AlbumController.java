package com.ihsm.esdp.photoGallery.gallery;

import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.photoGallery.photo.Photo;
import com.ihsm.esdp.users.security.MySecurityUser;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/gallery")
//@Data
@RequiredArgsConstructor
public class AlbumController {

    private final AlbumService albumService;

    private final ModelMapper mapper= new ModelMapper();

    @GetMapping("/display-all")
    public String displayAllGallery(Model model, Pageable pageable){
        Page<Album> albums = albumService.getAllGallery(pageable);
        model.addAttribute("albums",
                albums.stream()
                        .map(p -> mapper.map(p, Album.class))
                            .collect(Collectors.toList()));
        return "gallery/gallery";
    }

    @PostMapping("/creat-album")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String createGallery(@RequestParam String albumName, @RequestParam("file") MultipartFile file, Authentication auth) throws IOException {
        MySecurityUser user = (MySecurityUser) auth.getPrincipal();
        albumService.creatAlbum(albumName,file, user.getUser());
        return "redirect:/gallery/display-all";
    }

    @PostMapping("/remove")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String removeAlbum(@RequestParam Long id){
        albumService.removeAlbum(id);
        return "redirect:/gallery/display-all";
    }

    @GetMapping("/album/{id}")
    public String showAlbum(@PathVariable Long id,Model model,Pageable pageable){
        Optional<Album> albumById = this.albumService.getById(id);
        if (albumById.isEmpty()) return "redirect:/gallery/display-all";
        Page<Photo> photos = albumService.getPhotosByAlbum(id,pageable);
        model.addAttribute("photos",photos.
                stream()
                .map(a -> mapper.map(a, Photo.class))
                .collect(Collectors.toList()));
        model.addAttribute("albumId",id);
        model.addAttribute("album", mapper.map(albumById.get(), AlbumDto.class));
        return "gallery/album";
    }

}
