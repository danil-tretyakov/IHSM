package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.departments.Department;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;


@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employments_history")
public class Employment extends BaseEntity {

    @Column
    private LocalDate transferDateIn;

    @Column
    private LocalDate transferDateOut;

    @ManyToOne
    private Department department;

    @ManyToOne(cascade = CascadeType.MERGE)
    private EmployeePosition employeePosition;

    @ManyToOne
    private Employee employee;
}
