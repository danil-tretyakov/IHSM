package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.*;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "business_trips")
public class BusinessTrip extends BaseEntity {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private LocalDate dateOfBeginning;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private LocalDate dateOfEnding;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String purposeOfTheTrip;

    @Column
    private long orderNumber;

    @ManyToOne
    private Employee employee;
}
