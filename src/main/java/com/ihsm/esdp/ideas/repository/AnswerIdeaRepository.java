package com.ihsm.esdp.ideas.repository;

import com.ihsm.esdp.ideas.entities.AnswerIdea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerIdeaRepository extends JpaRepository<AnswerIdea,Long> {
}
