package com.ihsm.esdp.ideas.services;


import com.ihsm.esdp.exceptions.NotFoundException;
import com.ihsm.esdp.ideas.dtos.AnswerIdeaDTO;
import com.ihsm.esdp.ideas.entities.AnswerIdea;
import com.ihsm.esdp.ideas.entities.Idea;
import com.ihsm.esdp.ideas.repository.AnswerIdeaRepository;
import com.ihsm.esdp.ideas.repository.IdeaRepository;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.repositories.UserRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@Service
@RequiredArgsConstructor
public class AnswerIdeaService {
    private final AnswerIdeaRepository answerIdeaRepository;
    private final IdeaRepository ideaRepository;
    private final UserRepository userRepository;


    public void addAnswer(AnswerIdeaDTO answerIdeaDTO) {
        Idea idea = ideaRepository.findById(answerIdeaDTO.getIdea_id()).orElseThrow(NotFoundException::new);
        User user = userRepository.findById(answerIdeaDTO.getUser_id()).orElseThrow(NotFoundException::new);
        AnswerIdea answerIdea = AnswerIdea
                .builder()
                .answer(answerIdeaDTO.getAnswer())
                .idea(idea)
//                .employee(employee)
                .user(user)
                .build();
        answerIdeaRepository.save(answerIdea);

    }

    public List<AnswerIdea> getAll() {
        return answerIdeaRepository.findAll();
    }
}
