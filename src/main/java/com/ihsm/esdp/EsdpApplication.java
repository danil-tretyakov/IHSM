package com.ihsm.esdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class EsdpApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsdpApplication.class, args);
    }

}
