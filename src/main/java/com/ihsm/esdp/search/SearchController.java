package com.ihsm.esdp.search;

import com.ihsm.esdp.documents.entities.Document;
import com.ihsm.esdp.news.entities.News;
import com.ihsm.esdp.users.information.Employee;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
@AllArgsConstructor
public class SearchController {
    private final SearchService searchService;
    private final ModelMapper mapper= new ModelMapper();

    @GetMapping("/search")
    public String search(@RequestParam String name, Model model){
        final List<Document> documents = this.searchService.getSearchInDocuments(name);
        model.addAttribute("documents",documents.stream()
                .map(t-> mapper.map(t,Document.class))
                .distinct()
                .collect(Collectors.toList()));
        final List<Employee> employees = this.searchService.findEmployeesByParameter(name);
        model.addAttribute("employees",employees.stream()
                .map(t-> mapper.map(t,Employee.class))
                .distinct()
                .collect(Collectors.toList()));
        final List<News> news = this.searchService.getSearchNews(name);
        model.addAttribute("news",news.stream()
                .map(t-> mapper.map(t,News.class))
                .distinct()
                .collect(Collectors.toList()));
        return "search/search";
    }
}
