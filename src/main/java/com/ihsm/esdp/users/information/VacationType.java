package com.ihsm.esdp.users.information;

public enum VacationType {
    REGULAR("Очередной"),
    UNPAID("Неоплачиваемый"),
    TOCAREFORTHECHILD("По уходу за ребенком"),
    SICK("Больничный");

    private final String name;

    VacationType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
