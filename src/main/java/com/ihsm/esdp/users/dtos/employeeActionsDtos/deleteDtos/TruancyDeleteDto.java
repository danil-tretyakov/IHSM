package com.ihsm.esdp.users.dtos.employeeActionsDtos.deleteDtos;

import com.ihsm.esdp.users.information.Truancy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class TruancyDeleteDto extends Truancy {
    private boolean toDelete;
}
