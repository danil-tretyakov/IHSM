package com.ihsm.esdp.users.repositories;

import com.ihsm.esdp.users.information.ScienceDegree;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScienceDegreeRepository extends JpaRepository<ScienceDegree, Long> {
}
