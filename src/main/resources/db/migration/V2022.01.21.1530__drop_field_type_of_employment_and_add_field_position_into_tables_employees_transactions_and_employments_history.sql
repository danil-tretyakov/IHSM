alter table public.employees
    drop column type_of_employment_id;

alter table public.employments_history
    drop column type_of_employment_id;

alter table public.transactions
    drop column type_of_employment_id;

drop table public.type_of_employments;

alter table public.employees
    add column type_of_employment character varying(255) COLLATE pg_catalog."default",
    add column employee_position_id bigint;

alter table public.employments_history
    add column employee_position_id bigint;

alter table public.transactions
    add column employee_position_id bigint;