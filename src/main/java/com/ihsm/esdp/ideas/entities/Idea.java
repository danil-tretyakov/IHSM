package com.ihsm.esdp.ideas.entities;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.departments.Department;

import com.ihsm.esdp.ideas.enums.CategoryIdea;
import com.ihsm.esdp.users.User;
import lombok.*;

import javax.persistence.*;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "idea")
public class Idea extends BaseEntity {
    private String name;
    private String surname;
    private String position;
    private String department;
    private String ideaName;
    private String text;

    private String resource;

    private String realisation;

    @Enumerated(EnumType.STRING)
    @Column(name = "id_categoryIdea")
    private CategoryIdea categoryIdea;

    @OneToMany(mappedBy = "idea")
    private List<AnswerIdea> answerIdeas;

    @ManyToOne
    private User user;
}

