package com.ihsm.esdp.users.dtos;

import com.ihsm.esdp.users.information.ScienceDegree;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ScienceDegreeDto extends ScienceDegree {
    private boolean toDelete;
    private boolean isNew;
}
