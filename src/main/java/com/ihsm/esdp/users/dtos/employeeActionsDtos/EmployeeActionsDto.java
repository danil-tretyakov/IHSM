package com.ihsm.esdp.users.dtos.employeeActionsDtos;

import com.ihsm.esdp.users.dtos.employeeActionsDtos.deleteDtos.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeActionsDto {

    private long employeeId;

    private List<VacationDeleteDto> vacations;

    private List<TransactionDeleteDto> transactions;

    private List<VacationOrderDeleteDto> vacationOrders;

    private List<TruancyDeleteDto> truancyList;

    private List<BusinessTripDeleteDto> businessTrips;
}
