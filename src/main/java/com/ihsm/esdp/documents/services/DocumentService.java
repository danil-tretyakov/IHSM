package com.ihsm.esdp.documents.services;


import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.departments.DepartmentRepository;
import com.ihsm.esdp.documents.categories.CategoryDocs;
import com.ihsm.esdp.documents.repositories.DocumentRepository;
import com.ihsm.esdp.documents.categories.CategoryRepo;
import com.ihsm.esdp.documents.entities.Document;
import com.ihsm.esdp.exceptions.NotFoundException;
import com.ihsm.esdp.users.enums.CategoryOfNotification;
import com.ihsm.esdp.users.service.EmailSenderService;
import com.ihsm.esdp.utility.MaltyPartFile;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Service
@Data
@RequiredArgsConstructor
public class DocumentService {

    private final EmailSenderService emailSenderService;

    private final DocumentRepository documentRepository;

    private final DepartmentRepository departmentRepository;

    private final CategoryRepo categoryRepo;

    private final MaltyPartFile maltyPartFile;

    private final String uploadPath = "upload/doc";


    public Page<Document> getDocsByCategories(Long categoryId,Pageable pageable) {
        return this.documentRepository.findAllByCategoryDocsId(categoryId, pageable);
    }

    public List<Document> getAll(){
        return (List<Document>) documentRepository.findAll();
    }

    public List<Document> getAllByDeps(Long id) {
        return documentRepository.findAllByDepartment_Id(id);
    }

    public void createDocs(MultipartFile file, Long id, Long depId) throws IOException {
        CategoryDocs categoryDocs = categoryRepo.findById(id).orElseThrow(NotFoundException::new);
        Department dep = departmentRepository.findById(depId).orElseThrow(NotFoundException::new);
        Document document = Document.builder()
                .documentName(file.getOriginalFilename())
                .department(dep)
                .pathUploadFile(maltyPartFile.uploadFile(uploadPath, file))
                .categoryDocs(categoryDocs)
                .build();
        documentRepository.save(document);
        this.emailSenderService.sendNotificationToAllUsers(document.getDocumentName(), CategoryOfNotification.DOCUMENT, dep.getId(), dep.getName());
    }

    public void deleteDoc(Long id) {
        Document doc = documentRepository.findById(id).orElseThrow(NotFoundException::new);
        documentRepository.delete(doc);
    }

    public void createDocForGuidePage(MultipartFile multipartFile, String doc_type) throws IOException{
        this.documentRepository.save(Document.builder()
                .documentName(doc_type + "$" + multipartFile.getOriginalFilename())
                .fileDate(LocalDateTime.now())
                .pathUploadFile(maltyPartFile.uploadFile(uploadPath, multipartFile))
                .build());
    }

    public String getOriginalFilename(String filename) {
        String[] filenameSeparatedByDot = filename.split("\\.");
        List<String> listOfStrings = new ArrayList<>(Arrays.asList(filenameSeparatedByDot));
        listOfStrings.remove(0);
        return String.join(".", listOfStrings);
    }

    public ResponseEntity<ByteArrayResource> createDocumentResponseEntity(String disposition_type, String original_file_name, MediaType mediaType, int data_length, ByteArrayResource file) {
        ContentDisposition content_disposition = ContentDisposition.builder(disposition_type)
                .filename(original_file_name, StandardCharsets.UTF_8)
                .build();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, content_disposition.toString())
                .contentType(mediaType)
                .contentLength(data_length)
                .body(file);
    }
}
