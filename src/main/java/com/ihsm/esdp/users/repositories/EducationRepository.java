package com.ihsm.esdp.users.repositories;

import com.ihsm.esdp.users.information.Education;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EducationRepository extends JpaRepository<Education, Long> {
}
