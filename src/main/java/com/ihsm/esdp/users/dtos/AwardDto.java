package com.ihsm.esdp.users.dtos;

import com.ihsm.esdp.users.information.Award;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class AwardDto extends Award {
    private boolean toDelete;
    private boolean isNew;
}
