package com.ihsm.esdp.ideas.dtos;

import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.ideas.entities.AnswerIdea;
import com.ihsm.esdp.ideas.enums.CategoryIdea;
import lombok.*;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode()
public class IdeaFormDto {
    private Long id;
    private String name;
    private String surname;
    private String position;
    private Department department;
    private String ideaName;
    private String text;
    private String resource;
    private String realisation;
    private CategoryIdea categoryIdea;
    private List<AnswerIdea> answerIdeas;

}


