package com.ihsm.esdp.users.information;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.*;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "truancies")
public class Truancy extends BaseEntity {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private LocalDate date;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String reason;

    @ManyToOne
    private Employee employee;
}
