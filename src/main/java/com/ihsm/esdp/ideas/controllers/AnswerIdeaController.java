package com.ihsm.esdp.ideas.controllers;

import com.ihsm.esdp.ideas.dtos.AnswerIdeaDTO;
import com.ihsm.esdp.ideas.services.AnswerIdeaService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("/answers")
public class AnswerIdeaController {
    private final AnswerIdeaService answerIdeaService;


    @GetMapping("/answer")
    private String getAnswerIdeaForm(Model model) {
        model.addAttribute("answer", new AnswerIdeaDTO());
        model.addAttribute("answers", answerIdeaService.getAll());

        return "idea/bank-idea";
    }

    @PostMapping("/newAnswer")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String createIdea(@ModelAttribute("answer") AnswerIdeaDTO answerIdeaDTO){
        answerIdeaService.addAnswer(answerIdeaDTO);
        return "redirect:/ideas/idea";
    }
}
