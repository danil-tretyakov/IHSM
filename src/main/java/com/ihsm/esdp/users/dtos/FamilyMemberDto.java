package com.ihsm.esdp.users.dtos;

import com.ihsm.esdp.users.information.FamilyMember;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class FamilyMemberDto extends FamilyMember {
    private boolean toDelete;
    private boolean isNew;
}
