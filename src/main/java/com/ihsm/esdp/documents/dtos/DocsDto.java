package com.ihsm.esdp.documents.dtos;

import com.ihsm.esdp.departments.Department;

import com.ihsm.esdp.documents.categories.CategoryDocs;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocsDto {
    private Long id;
    private String documentName;
    private CategoryDocs category;
    private Department department;
    private LocalDateTime fileDate;
    private String pathUploadFile;
}
