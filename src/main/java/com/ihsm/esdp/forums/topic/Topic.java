package com.ihsm.esdp.forums.topic;

import com.ihsm.esdp.baseEntity.BaseEntity;
import com.ihsm.esdp.users.User;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "topics")
@NoArgsConstructor
@AllArgsConstructor
public class Topic extends BaseEntity {
    @Column
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    private String description;

    @Column
    @DateTimeFormat(pattern ="dd/mm/yy")
    private LocalDateTime localDateTime;

    public Topic(User user,String name, LocalDateTime localDateTime) {
        this.user = user;
        this.name = name;
        this.localDateTime = localDateTime;
    }
}
