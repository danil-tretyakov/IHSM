package com.ihsm.esdp.users.service;

import com.ihsm.esdp.exceptions.EmployeeNotFoundException;
import com.ihsm.esdp.users.dtos.employeeActionsDtos.EmployeeActionsDto;
import com.ihsm.esdp.users.dtos.employeeActionsDtos.deleteDtos.*;
import com.ihsm.esdp.users.information.*;
import com.ihsm.esdp.users.repositories.employeeActionsRepositories.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeActionsService {

    private final TransactionRepository transactionRepository;

    private final VacationRepository vacationRepository;

    private final VacationOrderRepository vacationOrderRepository;

    private final TruancyRepository truancyRepository;

    private final BusinessTripRepository businessTripRepository;

    private final EmployeeService employeeService;

    private final UserService userService;

    public void updateEmployeeActionsData(EmployeeActionsDto employeeActionsDto) throws EmployeeNotFoundException {
        if (this.employeeService.getEmployeeById(employeeActionsDto.getEmployeeId()).isPresent() && this.userService.findUserForEmployeeDataById(employeeActionsDto.getEmployeeId()).isPresent()){
            deleteRequiredTransactions(employeeActionsDto.getTransactions());
            deleteRequiredVacations(employeeActionsDto.getVacations());
            deleteRequiredTruancies(employeeActionsDto.getTruancyList());
            deleteRequiredVacationOrders(employeeActionsDto.getVacationOrders());
            deleteRequiredBusinessTrips(employeeActionsDto.getBusinessTrips());
            // логика удаления и сохранения всех данных
        }
        throw new EmployeeNotFoundException();
    }

    private void deleteRequiredTransactions(List<TransactionDeleteDto> transactions) {
        if (transactions != null && !transactions.isEmpty()){
            transactions.removeIf(transactionDeleteDto -> !transactionDeleteDto.isToDelete());
            for (TransactionDeleteDto transactionDeleteDto : transactions) {
                Optional<Transaction> optional = this.transactionRepository.findById(transactionDeleteDto.getId());
                optional.ifPresent(this.transactionRepository::delete);
            }
        }
    }

    private void deleteRequiredVacationOrders(List<VacationOrderDeleteDto> vacationOrders) {
        if (vacationOrders != null && !vacationOrders.isEmpty()){
            vacationOrders.removeIf(vacationOrderDeleteDto -> !vacationOrderDeleteDto.isToDelete());
            for (VacationOrder vacationOrder : vacationOrders) {
                Optional<VacationOrder> optional = this.vacationOrderRepository.findById(vacationOrder.getId());
                optional.ifPresent(this.vacationOrderRepository::delete);
            }
        }
    }

    private void deleteRequiredTruancies(List<TruancyDeleteDto> truancyList) {
        if (truancyList != null && !truancyList.isEmpty()){
            truancyList.removeIf(truancyDeleteDto -> !truancyDeleteDto.isToDelete());
            for (TruancyDeleteDto truancyDto : truancyList) {
                Optional<Truancy> optional = this.truancyRepository.findById(truancyDto.getId());
                optional.ifPresent(this.truancyRepository::delete);
            }
        }
    }

    private void deleteRequiredBusinessTrips(List<BusinessTripDeleteDto> businessTrips) {
        if (businessTrips != null && !businessTrips.isEmpty()){
            businessTrips.removeIf(businessTripDeleteDto -> !businessTripDeleteDto.isToDelete());
            for (BusinessTripDeleteDto businessTrip : businessTrips){
                Optional<BusinessTrip> optional = this.businessTripRepository.findById(businessTrip.getId());
                optional.ifPresent(this.businessTripRepository::delete);
            }
        }

    }

    private void deleteRequiredVacations(List<VacationDeleteDto> vacations) {
        if (vacations != null && !vacations.isEmpty()){
            vacations.removeIf(vacationDeleteDto -> !vacationDeleteDto.isToDelete());
            for (VacationDeleteDto vacationDto : vacations) {
                Optional<Vacation> optional = this.vacationRepository.findById(vacationDto.getId());
                optional.ifPresent(this.vacationRepository::delete);
            }
        }
    }

    public void deleteEmployeeInfoFromDB(Long employeeId) throws EmployeeNotFoundException {
        if (this.employeeService.getEmployeeById(employeeId).isPresent() && this.userService.findUserForEmployeeDataById(employeeId).isPresent()){
            this.employeeService.deleteEmployeeInfoFromDB(employeeId);
            return;
        }
        throw new EmployeeNotFoundException();
    }
}
