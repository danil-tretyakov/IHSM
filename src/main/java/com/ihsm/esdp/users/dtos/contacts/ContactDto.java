package com.ihsm.esdp.users.dtos.contacts;

import com.ihsm.esdp.departments.Department;
import com.ihsm.esdp.users.User;
import com.ihsm.esdp.users.information.EmployeePosition;
import com.ihsm.esdp.users.information.Subject;
import com.ihsm.esdp.users.information.TypeOfEmployment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactDto {
    private Long id;
    private User user;
    private String patronymic;
    private Department department;
    private TypeOfEmployment typeOfEmployment;
    private EmployeePosition employeePosition;
    private String phoneNumber;
    private String imagePath;
}
