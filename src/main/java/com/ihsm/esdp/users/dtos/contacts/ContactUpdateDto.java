package com.ihsm.esdp.users.dtos.contacts;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

@EqualsAndHashCode(callSuper = true)
@Data
public class ContactUpdateDto extends ContactDto {
    private MultipartFile image;
}
