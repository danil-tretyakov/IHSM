package com.ihsm.esdp.users.dtos.employeeActionsDtos.deleteDtos;

import com.ihsm.esdp.users.information.BusinessTrip;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class BusinessTripDeleteDto extends BusinessTrip {
    private boolean toDelete;
}
