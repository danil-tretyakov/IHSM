package com.ihsm.esdp.photoGallery.photo;

import com.ihsm.esdp.photoGallery.gallery.Album;
import com.ihsm.esdp.photoGallery.gallery.AlbumService;
import lombok.Data;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/photo")
@Data
public class PhotoController {

    private final PhotoService photoService;

    private final AlbumService albumService;

    @PostMapping("/addToAlbum/{id}")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String addPhoto(@PathVariable Long id, List<MultipartFile> file) throws IOException {
        photoService.addPhotoToAlbum(file,id);
        return "redirect:/gallery/album/{id}";
    }

    @GetMapping("/picture/{id}")
    public String showPhoto(@PathVariable Long id, Model model){
        Photo photo = photoService.getPhotoById(id);
        model.addAttribute("photo",photo);
        return "gallery/photo";
    }


    @PostMapping("/delete/{id}")
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_HR', 'ROLE_SUPER_ADMIN')")
    public String deletePicture(@PathVariable Long id, @RequestParam Long albumId){
        photoService.deletePhoto(id);
        Album album = albumService.getAlbum(albumId);
        return "redirect:/gallery/album/"+album.getId();

    }
}
