package com.ihsm.esdp.users.repositories;

import com.ihsm.esdp.users.information.FamilyMember;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamilyMemberRepository extends JpaRepository<FamilyMember, Long> {
}
