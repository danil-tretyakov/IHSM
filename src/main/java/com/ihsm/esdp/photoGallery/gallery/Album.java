package com.ihsm.esdp.photoGallery.gallery;

import com.ihsm.esdp.baseEntity.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name="albums")
public class Album extends BaseEntity {

    @Column(name = "name_of_album")
    private String albumName;

    @Column(name = "photo_path")
    private String photoPath;
}
