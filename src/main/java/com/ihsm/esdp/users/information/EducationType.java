package com.ihsm.esdp.users.information;

public enum EducationType {
    HIGHER("Высшее"), SECONDARY("Среднее"), PROFESSIONAL("Профессиональное"), ADDITIONAL("Дополнительное");

    private final String name;

    EducationType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
