package com.ihsm.esdp.users.repositories.employeeActionsRepositories;

import com.ihsm.esdp.users.information.Truancy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TruancyRepository extends JpaRepository<Truancy, Long> {
}
